﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PAT_Sample
{
    class Program
    {
        static  void Main(string[] args)
        {
            //GetBuilds();
            var client = new RestClient("http://hqtfs01:8080/tfs/SharePointCollection/Intranet%20Revamp/_apis/wit/queries?api-version=1.0");
            var request = new RestRequest(Method.GET);
            request.AddHeader("postman-token", "632910d3-37f5-871c-aa50-14539d15d13a");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Basic Onpld2p6bHl5aGRteHYyYWVsb3J3ZzN6aWtsd3RrMmpzZDR1YWFuYXBuajI1M2tnMnR5c2E=");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            Console.ReadLine();
        }


        public static async void GetBuilds()
        {
            try
            {
                var personalaccesstoken = "zewjzlyyhdmxv2aelorwg3ziklwtk2jsd4uaanapnj253kg2tysa";

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(
                        new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", "SpAcess", personalaccesstoken))));

                    using (HttpResponseMessage response = client.GetAsync("http://hqtfs01:8080/tfs/SharePointCollection/Intranet%20Revamp/_apis//wit/queries?api-version=4.1").Result)                    
                    {
                        response.EnsureSuccessStatusCode();
                        string responseBody = await response.Content.ReadAsStringAsync();
                        Console.WriteLine(responseBody);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
