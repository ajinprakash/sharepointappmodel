﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RestSharp;
using Newtonsoft.Json;
using System.Net;
using System.Web.Script.Serialization;

namespace ApiTestingApp
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var client = new RestClient("http://mtest.km.qa/WcfService3/GETEMPLOYEEATTENDANCEOVERALL");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "968d1d63-a85f-389d-ff23-3db79552e02b");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic a2FocmFtYWEuY29ycFx1bHRpbXVzOmFkbWluS005OQ==");
            request.AddHeader("accept", "application/json");
            request.AddParameter("application/json", "{\"SelectedDateFrom\": \"2018-05-05\",\"SelectedDateTo\": \"2018-06-06\",\"empID\": \"11816\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var resultObj = response.Content;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseToLog = new
                {
                    statusCode = response.StatusCode,
                    content = response.Content,
                    headers = response.Headers,
                    // The Uri that actually responded (could be different from the requestUri if a redirection occurred)
                    responseUri = response.ResponseUri,
                    errorMessage = response.ErrorMessage,
                };
                OverAllAttendenceData ro = JsonConvert.DeserializeObject<OverAllAttendenceData>(responseToLog.content);
                dataHolder.InnerHtml = ro.Transaction.Count.ToString();
            }

        }
    }
}