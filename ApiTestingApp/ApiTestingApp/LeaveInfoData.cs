﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiTestingApp
{
    
    public class Datum
    {
        public double LRAN8 { get; set; }
        public string DESCRIPTION { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TOIDATE { get; set; }
        public double DURATION { get; set; }
        public string FLAG { get; set; }
    }

    public class LeaveInfoData
    {
        public string errorCode { get; set; }
        public string ErrorDesc { get; set; }
        public bool HasRows { get; set; }
        public List<Datum> data { get; set; }
    }

}