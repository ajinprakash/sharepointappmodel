/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/bootstrap/index.d.ts" />
/// <reference path="../typings/jqueryui/jqueryui.d.ts" />
/// <reference path="../typings/microsoft-ajax/microsoft.ajax.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
/// <reference path="../sp-ts-csr.ts" />
var UserProfileFetch = (function () {
    function UserProfileFetch() {
        var hostweburl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._hostUrl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._currentContext = SP.ClientContext.get_current();
        this._hostContext = new SP.AppContextSite(this._currentContext, hostweburl);
        var appweburl = decodeURIComponent(this.getQueryStringParameter("SPAppWebUrl"));
        this._web = this._hostContext.get_web();
    }
    UserProfileFetch.prototype.getQueryStringParameter = function (urlParameterKey) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] == urlParameterKey)
                return decodeURIComponent(singleParam[1]);
        }
    };
    return UserProfileFetch;
}());
'use strict';
ExecuteOrDelayUntilScriptLoaded(pageload, "sp.js");
function pageload() {
    ExecuteOrDelayUntilScriptLoaded(fetchPrfileData, "sp.userprofiles.js");
}
function fetchPrfileData() {
    var foo = new UserProfileFetch();
    var peopleManager = new SP.UserProfiles.PeopleManager(foo._currentContext);
    var personProperties = peopleManager.getMyProperties();
    foo._currentContext.load(personProperties);
    foo._currentContext.executeQueryAsync(function (sender, args) {
        var properties = personProperties.get_userProfileProperties();
        var messageText = "";
        for (var key in properties) {
            messageText += "<br />[" + key + "]: \"" + properties[key] + "\"";
            if (key == "PictureURL") {
                $get("PictureURL").setAttribute('src', properties[key].toString().replace("MThumb", "LThumb"));
            }
            if (key == "PreferredName") {
                $get("PreferredName").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "Title") {
                $get("Title").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "Department") {
                $get("Department").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "WorkPhone") {
                $get("WorkPhone").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "WorkEmail") {
                $get("WorkEmail").innerText = properties[key].toString();
                $get("emailLink").setAttribute('href', "mailto:" + properties[key].toString());
            }
            if (key == "PersonalSpace") {
                $get("profileButton").setAttribute('href', properties[key].toString());
            }
            if (key == "Manager") {
                getManagerData(properties[key].toString(), peopleManager, foo._currentContext);
            }
        }
        $get("alldata").innerText = messageText;
        // $('.alldata').hide();
    }, function (sender, args) { alert('Error: ' + args.get_message()); });
}
function getManagerData(targetUser, peopleManager, context) {
    var deferred = $.Deferred();
    var personProperties = peopleManager.getPropertiesFor(targetUser);
    // Load the PersonProperties object and send the request.
    context.load(personProperties);
    context.executeQueryAsync(function () {
        var properties = personProperties.get_userProfileProperties();
        for (var key in properties) {
            if (key == "PreferredName") {
                $get("Manager").innerText = properties[key].toString().toUpperCase();
                deferred.resolve();
            }
        }
    }, function (sender, args) { alert('Error: ' + args.get_message()); deferred.reject(); });
    return deferred.promise();
}
//# sourceMappingURL=UserProfile.js.map