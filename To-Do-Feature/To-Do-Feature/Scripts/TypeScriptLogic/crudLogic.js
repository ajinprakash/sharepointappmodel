/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
/// <reference path="../typings/jqueryui/jqueryui.d.ts" />
/// <reference path="../typings/bootstrap/index.d.ts" />
var BasicOperations = (function () {
    function BasicOperations() {
        var hostweburl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._currentContext = SP.ClientContext.get_current();
        this._hostContext = new SP.AppContextSite(this._currentContext, hostweburl);
        var appweburl = decodeURIComponent(this.getQueryStringParameter("SPAppWebUrl"));
        this._web = this._hostContext.get_web();
        this._listName = "To-Do";
    }
    BasicOperations.prototype.GetCurrentUser = function () {
        var _this = this;
        this._currentuser = this._hostContext.get_web().get_currentUser();
        this._currentContext.load(this._currentuser);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessFullUserLoad(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessFullUserLoad = function () {
        console.log(this._currentuser.get_loginName());
        this.GetAllItems();
    };
    BasicOperations.prototype.getQueryStringParameter = function (urlParameterKey) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] == urlParameterKey)
                return decodeURIComponent(singleParam[1]);
        }
    };
    BasicOperations.prototype.GetAllItems = function () {
        var _this = this;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' /><Value Type='User'>" + this._currentuser.get_title() + "</Value></Eq></Where></Query></View>");
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccess(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccess = function () {
        var listItemEnumerator = this._listItems.getEnumerator();
        if ($('.emptyRow') != null) {
            $('.emptyRow').remove();
        }
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            $(".todoTable").append("<tr>" + "<td><input type='checkbox' class='chkdisable' id='chkBox" + currentItem.get_item('ID') + "' value='" + currentItem.get_item('ID') + "'></checkbox></td>" + "<td>" + "<b>" + currentItem.get_item('Description') + "</b>" + "</td>" + "<td>" + "<b>" + currentItem.get_item('Status') + "</b>" + "</td>" + "</tr>");
        }
    };
    BasicOperations.prototype.DeleteAllItems = function () {
        var _this = this;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' /><Value Type='User'>" + this._currentuser.get_title() + "</Value></Eq></Where></Query></View>");
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteAll(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessDeleteAll = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                var selector = "#chkBox" + currentItem.get_item('ID');
                $(selector).parent().parent().remove();
                this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteSingleItem(); }, function (sender, args) { return _this.logError(_this, args); });
            }
        }
        else {
            console.log("no tiem to delete!!");
        }
    };
    BasicOperations.prototype.OnSuccessDeleteSingleItem = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            var selector = "#chkBox" + currentItem.get_item('ID');
            $(selector).parent().parent().remove();
            this._currentContext.executeQueryAsync(function () { return _this.DeleteAllItems(); }, function (sender, args) { return _this.logError(_this, args); });
        }
    };
    BasicOperations.prototype.AppendItem = function (id) {
        var _this = this;
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessAppend(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessAppend = function () {
        var newAddedItem = this._newItem;
        if (newAddedItem != null) {
            if ($('.emptyRow') != null) {
                $('.emptyRow').remove();
            }
            $(".todoTable").append("<tr>" + "<td><input type='checkbox' class='chkdisable' id='chkBox" + newAddedItem.get_item('ID') + "' value='" + newAddedItem.get_item('ID') + "'></checkbox></td>" + "<td>" + "<b>" + newAddedItem.get_item('Description') + "</b>" + "</td>" + "<td>" + "<b>" + newAddedItem.get_item('Status') + "</b>" + "</td>" + "</tr>");
        }
    };
    BasicOperations.prototype.UpdateItem = function (id) {
        var _this = this;
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._newItem.set_item("Status", "Completed");
        this._newItem.update();
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessUpdate(id); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessUpdate = function (id) {
        var selector = "#chkBox" + id;
        $(selector).parent().parent().children().last().text("Completed");
    };
    BasicOperations.prototype.DeleteItem = function () {
        var _this = this;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var disabledItem = $('.chkdisable:checkbox:checked');
        if (disabledItem.length > 0) {
            for (var i = 0; i < disabledItem.length; i++) {
                var delID = $(disabledItem[i]).attr('value');
                $(disabledItem[i]).parent().parent().remove();
                this._newItem = this._list.getItemById(parseInt(delID));
                this._newItem.deleteObject();
                this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDelete(); }, function (sender, args) { return _this.logError(_this, args); });
            }
        }
    };
    BasicOperations.prototype.OnSuccessDelete = function () {
        this.DeleteItem();
    };
    BasicOperations.prototype.AddItem = function () {
        var _this = this;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        newListItem.set_item("Description", $("#txtTitle").val());
        newListItem.update();
        this._currentContext.load(newListItem);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessItemADD(newListItem.get_item("ID")); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessItemADD = function (id) {
        $("#txtTitle").val('');
        this.AppendItem(parseInt(id));
    };
    BasicOperations.prototype.logError = function (sender, args) {
        $(".message").html("Request Failed: " + args.get_message());
    };
    return BasicOperations;
}());
'use strict';
ExecuteOrDelayUntilScriptLoaded(pageload, "sp.js");
function pageload() {
    var foo = new BasicOperations();
    foo.GetCurrentUser();
    $(".btnSubmitItem").click(function () {
        foo.AddItem();
    });
    $(".todateCreate").click(function () {
        $("#myModal").modal();
    });
    $(".todateDelete").click(function () {
        foo.DeleteItem();
    });
    $(".todateDeleteAll").click(function () {
        foo.DeleteAllItems();
    });
    document.addEventListener('change', function (e) {
        if ($(e.target).prop('checked')) {
            //Do stuff
            console.log($(e.target).prop('checked'));
            $(e.target).prop("disabled", "disabled");
            $(e.target).parent().parent().children().css("background-color", "#b4afaf");
            $(e.target).parent().parent().children().css("color", "grey");
            foo.UpdateItem(parseInt($(e.target).val()));
        }
        else {
            console.log($(e.target).prop('checked'));
        }
    });
}
//# sourceMappingURL=crudLogic.js.map