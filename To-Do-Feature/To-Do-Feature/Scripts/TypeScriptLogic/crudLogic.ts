﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
/// <reference path="../typings/jqueryui/jqueryui.d.ts" />
/// <reference path="../typings/bootstrap/index.d.ts" />


class BasicOperations implements ICrudOperation, ILogger, ICrudAddOperation, ICrudAppendOperation, ICrudUpdateOperation, ICrudDeleteOperation, ICrudDeleteAllOperation, ICrudCurrentUserOperation {
    //Define properties with respect BasicOperations Class  
    _currentContext: SP.ClientContext;
    _hostContext: SP.AppContextSite;
    _web: SP.Web;
    _colList: SP.ListCollection;
    _list: SP.List;
    _listName: string;
    _listItems: SP.ListItemCollection;
    _newItem: SP.ListItem;
    query: SP.CamlQuery;
    _currentuser: SP.User;
    constructor() {
        let hostweburl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._currentContext = SP.ClientContext.get_current();
        this._hostContext = new SP.AppContextSite(this._currentContext, hostweburl);
        let appweburl = decodeURIComponent(this.getQueryStringParameter("SPAppWebUrl"));
        this._web = this._hostContext.get_web();
        this._listName = "To-Do";  
            
    }

    GetCurrentUser(): void {
        this._currentuser = this._hostContext.get_web().get_currentUser();
        this._currentContext.load(this._currentuser);
        this._currentContext.executeQueryAsync(() => this.OnSuccessFullUserLoad(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessFullUserLoad() {
        console.log(this._currentuser.get_loginName());
        this.GetAllItems();
    }
    private getQueryStringParameter(urlParameterKey: string) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] == urlParameterKey)
                return decodeURIComponent(singleParam[1]);
        }
    }



    GetAllItems(): void {
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' /><Value Type='User'>" + this._currentuser.get_title() + "</Value></Eq></Where></Query></View>");
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(() => this.OnSuccess(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccess() {
        var listItemEnumerator = this._listItems.getEnumerator();
        if ($('.emptyRow') != null) {
            $('.emptyRow').remove();
        }
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            $(".todoTable").append("<tr>" + "<td><input type='checkbox' class='chkdisable' id='chkBox" + currentItem.get_item('ID') + "' value='" + currentItem.get_item('ID') + "'></checkbox></td>" + "<td>" + "<b>" + currentItem.get_item('Description') + "</b>" + "</td>" + "<td>" + "<b>" + currentItem.get_item('Status') + "</b>" + "</td>" + "</tr>");
        }
    }


    DeleteAllItems(): void {
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml("<View><Query><Where><Eq><FieldRef Name='Author' /><Value Type='User'>" + this._currentuser.get_title() + "</Value></Eq></Where></Query></View>");
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(() => this.OnSuccessDeleteAll(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessDeleteAll() {
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                var selector = "#chkBox" + currentItem.get_item('ID');
                $(selector).parent().parent().remove();
                this._currentContext.executeQueryAsync(() => this.OnSuccessDeleteSingleItem(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
            }
        }
        else { console.log("no tiem to delete!!") }
    }

    private OnSuccessDeleteSingleItem() {
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            var selector = "#chkBox" + currentItem.get_item('ID');
            $(selector).parent().parent().remove();
            this._currentContext.executeQueryAsync(() => this.DeleteAllItems(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
        }
    }

    AppendItem(id): void {
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(() => this.OnSuccessAppend(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessAppend() {
        var newAddedItem = this._newItem;
        if (newAddedItem != null) {
            if ($('.emptyRow') != null) {
                $('.emptyRow').remove();
            }
            $(".todoTable").append("<tr>" + "<td><input type='checkbox' class='chkdisable' id='chkBox" + newAddedItem.get_item('ID') + "' value='" + newAddedItem.get_item('ID') + "'></checkbox></td>" + "<td>" + "<b>" + newAddedItem.get_item('Description') + "</b>" + "</td>" + "<td>" + "<b>" + newAddedItem.get_item('Status') + "</b>" + "</td>" + "</tr>");
        }

    }

    UpdateItem(id): void {
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._newItem.set_item("Status", "Completed");
        this._newItem.update();
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(() => this.OnSuccessUpdate(id), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessUpdate(id) {
        var selector = "#chkBox" + id;
        $(selector).parent().parent().children().last().text("Completed");

    }

    DeleteItem(): void {
        this._list = this._web.get_lists().getByTitle(this._listName);
        var disabledItem = $('.chkdisable:checkbox:checked')
        if (disabledItem.length > 0) {
            for (var i = 0; i < disabledItem.length; i++) {
                var delID = $(disabledItem[i]).attr('value');
                $(disabledItem[i]).parent().parent().remove();
                this._newItem = this._list.getItemById(parseInt(delID));
                this._newItem.deleteObject();
                this._currentContext.executeQueryAsync(() => this.OnSuccessDelete(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
            }
        }
    }
    private OnSuccessDelete() {
        this.DeleteItem();

    }

    AddItem(): void {
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        newListItem.set_item("Description", $("#txtTitle").val());
        newListItem.update();
        this._currentContext.load(newListItem);
        this._currentContext.executeQueryAsync(() => this.OnSuccessItemADD(newListItem.get_item("ID")), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessItemADD(id) {
        $("#txtTitle").val('');
        this.AppendItem(parseInt(id));
    }


    logError(sender, args) {
        $(".message").html("Request Failed: " + args.get_message());
    }

}

interface ICrudOperation {
    GetAllItems(): void;
}

interface ICrudAppendOperation {
    AppendItem(id): void;
}

interface ICrudUpdateOperation {
    UpdateItem(id): void;
}
interface ICrudAddOperation {
    AddItem(): void;
}

interface ICrudDeleteOperation {
    DeleteItem(): void;
}

interface ICrudDeleteAllOperation {
    DeleteAllItems(): void;
}

interface ICrudCurrentUserOperation {
    GetCurrentUser(): void;
}

interface ILogger {
    logError(sender: any, e: SP.ClientRequestFailedEventArgs): void;
}

'use strict';
ExecuteOrDelayUntilScriptLoaded(pageload, "sp.js");
function pageload() {
    let foo: BasicOperations = new BasicOperations();  
    foo.GetCurrentUser();
    $(".btnSubmitItem").click(function () {
        foo.AddItem();
    });
    $(".todateCreate").click(function () {
        $("#myModal").modal();
    });
    $(".todateDelete").click(function () {
        foo.DeleteItem();
    });
    $(".todateDeleteAll").click(function () {
        foo.DeleteAllItems();
    });
    document.addEventListener('change', function (e) {
        if ($(e.target).prop('checked')) {
            //Do stuff
            console.log($(e.target).prop('checked'));
            $(e.target).prop("disabled", "disabled");
            $(e.target).parent().parent().children().css("background-color", "#b4afaf");
            $(e.target).parent().parent().children().css("color", "grey");
            foo.UpdateItem(parseInt($(e.target).val()));
        } else {
            console.log($(e.target).prop('checked'));
        }
    });
}





