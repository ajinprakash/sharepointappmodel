﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace ApiTesting2
{
    public partial class Attendence : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                trigger_OverAllAttendeceFetch_Process();
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region DataFetchOperations      
        protected KahramaaDataReciever.OverAllAttendenceData fetchOverAllAttendenceData()
        {
            try
            {
                KahramaaDataReciever.KahramaData dataCallObject = new KahramaaDataReciever.KahramaData();
                var rawjsonData = dataCallObject.getAttendceOverAll(getWeekStartDate(), getWeekEndtDate(), "11816");
                if (rawjsonData != null)
                {
                    return rawjsonData;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Data Process Functions
        protected List<KahramaaDataReciever.Transaction> processAttendence()
        {
            try
            {
                var data = fetchOverAllAttendenceData();
                if (data != null)
                {
                    var transactions = new List<KahramaaDataReciever.Transaction>();
                    transactions = data.Transaction;
                    if (transactions.Count > 0)
                    {
                        return transactions;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }

        #endregion

        #region TriggerFunctions
        protected void trigger_OverAllAttendeceFetch_Process()
        {
            try
            {
                var transcations = processAttendence();
                if (transcations != null)
                {
                    createAttendenceTable(transcations);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region
        protected void createAttendenceTable(List<KahramaaDataReciever.Transaction> currentUserTransactions)
        {
            try
            {
                if (currentUserTransactions != null)
                {
                    if (currentUserTransactions.Count > 0)
                    {
                        double myAverage = 0;
                        foreach (var transaction in currentUserTransactions)
                        {
                            if (transaction.CheckIn.ToString("dd-MM-yyyy") == DateTime.Today.ToString("dd-MM-yyyy"))
                            {
                                hDateRight.InnerText = transaction.CheckIn.DayOfWeek.ToString() + ", " + transaction.CheckIn.Day.ToString() + " " + transaction.CheckIn.ToString("MMMM") + " " + transaction.CheckIn.Year;
                                pIntime.InnerText = "In Time :" + transaction.CheckIn.ToString("hh:mm tt");
                                pOuttime.InnerText = "Out Time :" + transaction.CheckOut.ToString("hh:mm tt");
                            }
                            HtmlTableRow tr = new HtmlTableRow();

                            HtmlTableCell tdDate = new HtmlTableCell();
                            tdDate.InnerText = transaction.CheckIn.ToString("dd-MM-yyyy");
                            tr.Controls.Add(tdDate);

                            HtmlTableCell tdCheckin = new HtmlTableCell();
                            tdCheckin.InnerText = transaction.CheckIn.TimeOfDay.Hours + ":" + transaction.CheckIn.TimeOfDay.Minutes;
                            tr.Controls.Add(tdCheckin);

                            HtmlTableCell tdCheckOut = new HtmlTableCell();
                            tdCheckOut.InnerText = transaction.CheckOut.TimeOfDay.Hours + ":" + transaction.CheckOut.TimeOfDay.Minutes;
                            tr.Controls.Add(tdCheckOut);


                            HtmlTableCell tdTotalHours = new HtmlTableCell();
                            TimeSpan tHours;
                            TimeSpan.TryParse(transaction.TotalHours, out tHours);

                            tdTotalHours.InnerText = tHours.Hours.ToString() + ":" + tHours.Minutes;
                            tr.Controls.Add(tdTotalHours);

                            HtmlTableCell tdlogImage = new HtmlTableCell();
                            if (tHours.Hours >= 8)
                            {
                                tdlogImage.InnerHtml = string.Format("<img src='Assets/images/ok_image.png' style='width:30px' />");
                            }
                            else if (tHours.Hours < 8)
                            {
                                tdlogImage.InnerHtml = string.Format("<img src='Assets/images/checkmark.png' style='width:25px' />");
                            }
                            else
                            {
                                tdlogImage.InnerHtml = string.Format("<img src='Assets/images/warning.png' style='width:25px' />");
                            }
                            tr.Controls.Add(tdlogImage);
                            tr.Attributes.Add("Class", "trdatatable");
                            tlbWeekAttendenceLog.Rows.Add(tr);
                            myAverage = myAverage + Convert.ToDouble(tHours.TotalHours);

                        }
                        divMonth.InnerText = DateTime.Now.ToString("MMMM");
                        divMyAvg.InnerText = Math.Round(Convert.ToDouble(myAverage / currentUserTransactions.Count), 1).ToString();
                        divTeamAverage.InnerText = Math.Round(Convert.ToDouble(myAverage / currentUserTransactions.Count), 1).ToString();

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region utility

        protected DateTime getWeekStartDate()
        {
            try
            {
                DateTime baseDate = DateTime.Today;
                var today = baseDate;
                var yesterday = baseDate.AddDays(-1);
                var thisWeekStart = baseDate.AddDays(0);
                if (baseDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    int i = 0;
                    while (thisWeekStart.DayOfWeek != DayOfWeek.Sunday)
                    {
                        thisWeekStart = baseDate.AddDays(i);
                        i--;
                    }
                }

                return thisWeekStart;
            }
            catch (Exception)
            {

                throw;
            }
        }
        protected DateTime getWeekEndtDate()
        {
            try
            {
                DateTime baseDate = DateTime.Today;
                var today = baseDate;
                var yesterday = baseDate.AddDays(-1);
                var thisWeekEnd = baseDate.AddDays(0);
                if (baseDate.DayOfWeek != DayOfWeek.Thursday)
                {
                    int i = 0;
                    thisWeekEnd = baseDate.AddDays(i);
                    while (thisWeekEnd.Equals(DayOfWeek.Thursday))
                    {
                        thisWeekEnd = baseDate.AddDays(i);
                        i++;
                    }
                }
                return thisWeekEnd;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}