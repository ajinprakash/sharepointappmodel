﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using Newtonsoft.Json;
using Json;
using System.Net;


namespace KahramaaDataReciever
{
    public class KahramaData
    {
        public string LeaveOverAllInfoUrl { get; set; }
        public string LeaveBalanceInfoUrl { get; set; }
        public string LeaveInfoUrl { get; set; }
        public KahramaData()
        {
            LeaveOverAllInfoUrl = "http://mtest.km.qa/WcfService3/GETEMPLOYEEATTENDANCEOVERALL";
            LeaveBalanceInfoUrl = "http://mtest.km.qa/WcfService3/GetALandCLBalance";
            LeaveInfoUrl = "http://mtest.km.qa/WcfService3/GETEMPLEAVEINFO";

        }

        #region Over All Attendence Region
        public OverAllAttendenceData getAttendceOverAll(DateTime startdate, DateTime endDate, string empID)
        {
            try
            {
                var attendenceData = new AttendenceOverAlldata()
                {
                    SelectedDateFrom = startdate.ToString("yyyy-MM-dd"),
                    SelectedDateTo = endDate.ToString("yyyy-MM-dd"),
                    empID = empID

                };

                string requestBody = JsonConvert.SerializeObject(attendenceData);
                var client = new RestClient(this.LeaveOverAllInfoUrl);
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Basic a2FocmFtYWEuY29ycFx1bHRpbXVzOmFkbWluS005OQ==");
                request.AddParameter("application/json", requestBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseToLog = new
                    {
                        statusCode = response.StatusCode,
                        content = response.Content,
                        headers = response.Headers,
                        // The Uri that actually responded (could be different from the requestUri if a redirection occurred)
                        responseUri = response.ResponseUri,
                        errorMessage = response.ErrorMessage,
                    };
                    KahramaaDataReciever.OverAllAttendenceData ro = JsonConvert.DeserializeObject<OverAllAttendenceData>(responseToLog.content);
                    return ro;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Leave Balance Region
        public LeaveBalanceData getLeaveBalanceAll(string empID)
        {
            try
            {
                var leaveData = new LeaveBalanace()
                {                   
                    empId = empID

                };

                string requestBody = JsonConvert.SerializeObject(leaveData);
                var client = new RestClient(this.LeaveBalanceInfoUrl);
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Basic a2FocmFtYWEuY29ycFx1bHRpbXVzOmFkbWluS005OQ==");
                request.AddHeader("Accept", "application/javascript");
                request.AddParameter("application/json", requestBody, ParameterType.RequestBody);               
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseToLog = new
                    {
                        statusCode = response.StatusCode,
                        content = response.Content,
                        headers = response.Headers,
                        // The Uri that actually responded (could be different from the requestUri if a redirection occurred)
                        responseUri = response.ResponseUri,
                        errorMessage = response.ErrorMessage,
                    };
                    KahramaaDataReciever.LeaveBalanceData ro = JsonConvert.DeserializeObject<LeaveBalanceData>((Newtonsoft.Json.JsonConvert.SerializeObject(responseToLog.content)));
                    return ro;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Leave Info Region
        public LeaveInfoData getLeaveInfo(string empID,string year)
        {
            try
            {
                var leaveData = new LeaveInfo()
                {
                    empID = empID,
                    year=year

                };

                string requestBody = JsonConvert.SerializeObject(leaveData);
                var client = new RestClient(this.LeaveInfoUrl);
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Basic a2FocmFtYWEuY29ycFx1bHRpbXVzOmFkbWluS005OQ==");
                request.AddParameter("application/json", requestBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);               
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseToLog = new
                    {
                        statusCode = response.StatusCode,
                        content = response.Content,
                        headers = response.Headers,
                        // The Uri that actually responded (could be different from the requestUri if a redirection occurred)
                        responseUri = response.ResponseUri,
                        errorMessage = response.ErrorMessage,
                    };
                    KahramaaDataReciever.LeaveInfoData ro = JsonConvert.DeserializeObject<LeaveInfoData>(responseToLog.content);
                    return ro;
                }
                else
                {
                    return null;
                }
                
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

    }

    public class AttendenceOverAlldata
    {
        public string SelectedDateFrom { get; set; }
        public string SelectedDateTo { get; set; }
        public string empID { get; set; }
    }

    public class LeaveBalanace
    {       
        public string empId { get; set; }
    }

    public class LeaveInfo
    {
        public string empID { get; set; }

        public string year { get; set; }
    }
}