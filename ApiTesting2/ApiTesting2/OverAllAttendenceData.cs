﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KahramaaDataReciever
{
    public class Transaction
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public string TotalHours { get; set; }
        public int getCount()
        {
            int count = this.GetType().GetProperties().Count();
            // or
            return count;
        }

    }

    public class OverAllAttendenceData
    {
        public string errorCode { get; set; }
        public string ErrorDesc { get; set; }
        public bool HasRows { get; set; }
        public string Absent { get; set; }
        public string Average { get; set; }
        public string Early { get; set; }
        public string EmployeeNumber { get; set; }
        public string Late { get; set; }
        public string Leave { get; set; }
        public string Training { get; set; }
        public List<Transaction> Transaction { get; set; }
    }
}