﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Leaves.aspx.cs" Inherits="ApiTesting2.Leaves" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="Assets/css/Style.css" rel="stylesheet" />
    <script type="text/javascript">

        var jsonString;
        function populateGraph(jsonString) {

            var jsonObject = JSON.parse(jsonString);
            console.log(jsonObject.d.results);

            var testdata = [];

            $.each(jsondata.d.results, function (key, value) {
                testdata.push({ y: Number(value.AvailableLeave), name: value.Title });
            });
            var options = {

                exportEnabled: true,

                animationEnabled: true,

                title: {

                    text: "Leave Report"

                },

                legend: {

                    horizontalAlign: "right",

                    verticalAlign: "center"

                },

                data: [{

                    type: "pie",

                    showInLegend: true,

                    toolTipContent: "<b>{name}</b>: ${y} (#percent%)",

                    indexLabel: "{name}",

                    legendText: "{name} (#percent%)",

                    indexLabelPlacement: "inside",

                    dataPoints: testdata

                }]

            };

            $("#chartContainer").CanvasJSChart(options);


        }

    </script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div id="chartContainer" style="height: 300px; width: 100%;"></div>

                        </div>
                    </div>
                    <div class="row">
                        <table class="table" runat="server" id="tlbAttendenceData">
                            <thead>
                                <tr>
                                    <th>DESCRIPTION</th>
                                    <th>FROM DATE</th>
                                    <th>TO DATE</th>
                                    <th>DURATION</th>
                                    <th>STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
