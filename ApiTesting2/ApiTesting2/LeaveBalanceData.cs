﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KahramaaDataReciever
{
    public class LeaveBalanceData
    {
        public string PropertyDescription { get; set; }
        public string PropertyDescriptionAR { get; set; }
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
    }
    
}