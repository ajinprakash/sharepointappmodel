﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attendence.aspx.cs" Inherits="ApiTesting2.Attendence" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="Assets/css/Style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="scriptManager1"></asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="panelTableAttendence">
            <ContentTemplate>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title attendenceLabel">ATTENDENCE</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10 ">
                            <div class=" col-sm-5 card weekContainer">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title" runat="server" id="divMonth"></h5>
                                                    <a href="#" class="btn btn-primary">Avg Hours/Day</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <h5 class="card-title">ME</h5>
                                                <p class="card-text" runat="server" id="divMyAvg"></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <h5 class="card-title">My Team</h5>
                                                <p class="card-text" runat="server" id="divTeamAverage"></p>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-sm-2 gap">
                                <div class="card">
                                    <div class="card-body">
                                    </div>
                                </div>
                            </div>

                            <div class="card col-sm-5 timingSummary">
                                <div class="card-body">
                                    <div class="row">
                                        <h5 class="card-title" runat="server" id="hDateRight"></h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <p class="card-text" id="pIntime" runat="server">
                                            </p>
                                        </div>
                                        <div class="col-sm-2">
                                            <table class="seperator">
                                                <tr>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-sm-5">
                                            <p class="card-text" id="pOuttime" runat="server">
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <table class="table" runat="server" id="tlbWeekAttendenceLog">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Log In</th>
                                    <th>Log Out</th>
                                    <th>Effective Hours</th>
                                    <th>Log</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
