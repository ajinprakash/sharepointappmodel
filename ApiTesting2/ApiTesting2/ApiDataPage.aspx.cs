﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RestSharp;
using Newtonsoft.Json;
using Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Text;

namespace ApiTesting2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        static string newdata=string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //getting over all attendecen data
                      

        }

        protected void displayContent(IRestResponse response)
        {
            try
            {
                if (response != null)
                {
                    leavedata.InnerText = response.Content;

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void btnGetAttendenceInfo_Click(object sender, EventArgs e)
        {
            try
            {
                leavedata.InnerHtml = string.Empty;
                var overAllAttendenceObject = new KahramaaDataReciever.KahramaData();
                var data = overAllAttendenceObject.getAttendceOverAll(Convert.ToDateTime("2018-05-05"), Convert.ToDateTime("2018-06-06"), "11816");
                leavedata.InnerHtml = JsonConvert.SerializeObject(data);

            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void btngetLeaveBalance_Click(object sender, EventArgs e)
        {
            try
            {
                leavedata.InnerHtml = string.Empty;
                var overAllAttendenceObject = new KahramaaDataReciever.KahramaData();
                var data = overAllAttendenceObject.getLeaveBalanceAll("11816");
                leavedata.InnerHtml = JsonConvert.SerializeObject(data);

            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void btnGetLeaveInfo_Click(object sender, EventArgs e)
        {
            try
            {
                leavedata.InnerHtml = string.Empty;
                var overAllAttendenceObject = new KahramaaDataReciever.KahramaData();
                var data = overAllAttendenceObject.getLeaveInfo("11816", "2018");
                leavedata.InnerHtml = data.ToString();

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}