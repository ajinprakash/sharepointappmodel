﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApiDataPage.aspx.cs" Inherits="ApiTesting2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
      
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel id="upanel" runat="server">
            <ContentTemplate>
                <div>
                    <asp:Button runat="server" ID="btnGetAttendenceInfo" Text="Get Attendence Over All Info" OnClick="btnGetAttendenceInfo_Click" />
                     <asp:Button runat="server" ID="btngetLeaveBalance" Text="Get Leave Balance Info" OnClick="btngetLeaveBalance_Click" />
                     <asp:Button runat="server" ID="btnGetLeaveInfo" Text="Get Leave Details Info -Year" OnClick="btnGetLeaveInfo_Click"  />
                    <div id="leavedata" runat="server"></div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGetAttendenceInfo" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
          
    </form>

</body>
</html>
