﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Newtonsoft;

namespace ApiTesting2
{
    public partial class Leaves : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            fetchLeaveBalance();
            trigger_OverAllAttendeceFetch_Process();
        }




        #region DataFetchOperations    

        protected void fetchLeaveBalance()
        {
            try
            {
                
                KahramaaDataReciever.KahramaData dataCallObject = new KahramaaDataReciever.KahramaData();
                var rawjsonData = dataCallObject.getLeaveBalanceAll("11816");
                if (rawjsonData != null)
                {
                    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(rawjsonData);                    
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        protected KahramaaDataReciever.LeaveInfoData fetchLeaveTransactions()
        {
            try
            {
                KahramaaDataReciever.KahramaData dataCallObject = new KahramaaDataReciever.KahramaData();
                var rawjsonData = dataCallObject.getLeaveInfo("11816", DateTime.Now.Year.ToString());
                if (rawjsonData != null)
                {
                    return rawjsonData;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

       
        #endregion

        #region Trigger Functions
        protected List<KahramaaDataReciever.Datum> processLeave()
        {
            try
            {
                var data = fetchLeaveTransactions();
                if (data != null)
                {
                    var transactions = new List<KahramaaDataReciever.Datum>();
                    transactions = data.data;
                    if (transactions.Count > 0)
                    {
                        return transactions;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }

        #endregion

        #region TriggerFunctions
        protected void trigger_OverAllAttendeceFetch_Process()
        {
            try
            {
                var transcations = processLeave();
                if (transcations != null)
                {
                    createAttendenceTable(transcations);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region
        protected void createAttendenceTable(List<KahramaaDataReciever.Datum> currentUserTransactions)
        {
            try
            {
                if (currentUserTransactions != null)
                {
                    if (currentUserTransactions.Count > 0)
                    {
                        foreach (var transaction in currentUserTransactions)
                        {
                            HtmlTableRow tr = new HtmlTableRow();

                            HtmlTableCell tdDate = new HtmlTableCell();
                            tdDate.InnerText = transaction.DESCRIPTION.ToString();
                            tr.Controls.Add(tdDate);

                            HtmlTableCell tdCheckin = new HtmlTableCell();
                            tdCheckin.InnerText = transaction.FROMDATE.ToString("dd-MM-yyyy");
                            tr.Controls.Add(tdCheckin);

                            HtmlTableCell tdCheckOut = new HtmlTableCell();
                            tdCheckOut.InnerText = transaction.TOIDATE.ToString("dd-MM-yyyy"); 
                            tr.Controls.Add(tdCheckOut);


                            HtmlTableCell tdTotalHours = new HtmlTableCell();
                            tdTotalHours.InnerText =transaction.DURATION.ToString();
                            tr.Controls.Add(tdTotalHours);

                            HtmlTableCell tdFlag = new HtmlTableCell();
                            tdFlag.InnerText = transaction.FLAG.ToString();
                            tr.Controls.Add(tdFlag);
                                                       
                            tr.Controls.Add(tdFlag);
                            tr.Attributes.Add("Class", "trdatatable");
                            tlbAttendenceData.Rows.Add(tr);


                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region utility

        protected DateTime getWeekStartDate()
        {
            try
            {
                DateTime baseDate = DateTime.Today.AddDays(-3);
                var today = baseDate;
                var yesterday = baseDate.AddDays(-1);
                var thisWeekStart = baseDate.AddDays(0);
                if (baseDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    int i = 0;
                    while (thisWeekStart.DayOfWeek != DayOfWeek.Sunday)
                    {
                        thisWeekStart = baseDate.AddDays(i);
                        i--;
                    }
                }

                return thisWeekStart;
            }
            catch (Exception)
            {

                throw;
            }
        }
        protected DateTime getWeekEndtDate()
        {
            try
            {
                DateTime baseDate = DateTime.Today.AddDays(-3);
                var today = baseDate;
                var yesterday = baseDate.AddDays(-1);
                var thisWeekEnd = baseDate.AddDays(0);
                if (baseDate.DayOfWeek != DayOfWeek.Thursday)
                {
                    int i = 0;
                    thisWeekEnd = baseDate.AddDays(i);
                    while (thisWeekEnd.Equals(DayOfWeek.Thursday))
                    {
                        thisWeekEnd = baseDate.AddDays(i);
                        i++;
                    }
                }
                return thisWeekEnd;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}