﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="68620ca6-7aa1-40d6-8eb8-9ba280ee920d" description="SharePoint Add-in Feature" featureId="68620ca6-7aa1-40d6-8eb8-9ba280ee920d" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="DemoSharePointOfficeApp" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="df10533c-fba6-4dab-b117-721de6e685b1" />
    <projectItemReference itemId="1549a6d4-f4da-479c-90e3-78f9a1b2b85f" />
    <projectItemReference itemId="eeacdbed-fc70-435f-a977-792a5ca0eae4" />
    <projectItemReference itemId="41b15edd-efa5-4535-a78e-8244040a9d15" />
    <projectItemReference itemId="e9ad044b-688e-43eb-8d42-e2dfffaf317f" />
    <projectItemReference itemId="1dfa20f8-cb28-41e9-bdef-2ef4d3ecb98a" />
    <projectItemReference itemId="3991b0c7-0527-4df7-8ae6-044651b1c170" />
    <projectItemReference itemId="c4524906-484d-459d-881b-2d6458b866ab" />
    <projectItemReference itemId="d9457eab-245b-4e6c-94f6-49db4754c328" />
    <projectItemReference itemId="b3ec8248-8404-49a0-b792-3d575af4b196" />
    <projectItemReference itemId="effa0e04-4dab-43f8-8937-384249a32f31" />
  </projectItems>
</feature>