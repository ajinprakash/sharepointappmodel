﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <SharePoint:ScriptLink Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
    <script type="text/javascript" src="../Scripts/TypeScriptForm/CSRNewForm.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
        <p id="status"></p>
    </div>
    <WebPartPages:AllowFraming ID="AllowFraming" runat="server" />
    <div id="WebPartHolder">
        <h2 style="display: inline-block;">Feedback Tracker List</h2>
        <a style="font-size: larger; padding-left: 15px;" id="newFormHref" href="">New Item</a>
        <a style="font-size: larger; padding-left: 15px;" id="settingsHref" href="">Settings</a>
        <WebPartPages:WebPartZone runat="server" FrameType="None" ID="MainZone" Title="loc:full">
            <WebPartPages:XsltListViewWebPart ID="XsltListViewWebPart_FeedbackTrackerNew" runat="server" ListUrl="Lists/FeedbackTrackerList" IsIncluded="True"  NoDefaultStyle="TRUE" Title="Feedback Tracker" PageType="PAGE_NORMALVIEW" Default="True" ViewContentTypeId="0x">
                    
            </WebPartPages:XsltListViewWebPart>
            
        </WebPartPages:WebPartZone>
    </div>
</asp:Content>
