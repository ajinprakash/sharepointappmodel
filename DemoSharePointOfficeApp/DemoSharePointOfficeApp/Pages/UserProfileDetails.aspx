﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />

<html>
<head>
    <title></title>

    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>   
     <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>

     <SharePoint:ScriptLink ID="ScriptLink1" Name="init.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink2" Name="sp.init.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink3" Name="sp.runtime.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink4" Name="sp.core.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink5" Name="sp.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink6" Name="SP.UserProfiles.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>


    <script type="text/javascript">
        // Set the style of the client web part page to be consistent with the host web.
        (function () {
            'use strict';

            var hostUrl = '';
            var link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            if (document.URL.indexOf('?') != -1) {
                var params = document.URL.split('?')[1].split('&');
                for (var i = 0; i < params.length; i++) {
                    var p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split('=')[1];
                        link.setAttribute('href', hostUrl + '/_layouts/15/defaultcss.ashx');
                        break;
                    }
                }
            }
            if (hostUrl == '') {
                link.setAttribute('href', '/_layouts/15/1033/styles/themable/corev15.css');
            }
            document.head.appendChild(link);
            var scriptbase = hostUrl + "/_layouts/15/";
            $.getScript(scriptbase + "MicrosoftAjax.js",
                function () {
                    $.getScript(scriptbase + "SP.js",
                        function () {
                            $.getScript(scriptbase + "SP.RequestExecutor.js", initContext);
                        });
                });
        })();

        var context;
        var user;
        function initContext() {
             context = SP.ClientContext.get_current();
             user = context.get_web().get_currentUser();

            // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
            $(document).ready(function () {
                try {
                    getUserName();
                    
                } catch (e) {
                    console.log(e.message);
                }


            });
        }

        function getUserName() {
            context.load(user);
            context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
        }

        // This function is executed if the above call is successful
        // It replaces the contents of the 'message' element with the user name
        var details;
        function onGetUserNameSuccess() {
            details = user.get_title() + "," + user.get_email() + "," + user.get_loginName() + ","
            $('#message').text('Hello ' + details.toString());
            getUserProperties(user.get_loginName());


        }

        // This function is executed if the above call fails
        function onGetUserNameFail(sender, args) {
            alert('Failed to get user name. Error:' + args.get_message());
        }

        var personProperties;
        function getUserProperties(userLogin) {

            // Replace the placeholder value with the target user's credentials.
            var targetUser = userLogin;

            // Get the current client context and PeopleManager instance.
             context = new SP.ClientContext.get_current();
            var peopleManager = new SP.UserProfiles.PeopleManager(context);

            // Get user properties for the target user.
            // To get the PersonProperties object for the current user, use the
            // getMyProperties method.
            personProperties = peopleManager.getPropertiesFor(targetUser);

            // Load the PersonProperties object and send the request.
            context.load(personProperties);
            context.executeQueryAsync(onRequestSuccess, onRequestFail);
        }

        // This function runs if the executeQueryAsync call succeeds.
        function onRequestSuccess() {

            // Get a property directly from the PersonProperties object.
            details += "DisplayName property is "
                + personProperties.get_displayName();

            // Get a property from the UserProfileProperties property.
            details += " DisplayName property is "
                + personProperties.get_userProfileProperties()['Department'] + "--" + personProperties.get_userProfileProperties()['Manager'] + "--" + personProperties.get_personalSiteHostUrl + "--" + personProperties.get_pictureUrl();
            $('#message').text(details);
            $('#profileImage').attr('src', personProperties.get_pictureUrl().toString().split('?')[0]);
        }

        // This function runs if the executeQueryAsync call fails.
        function onRequestFail(sender, args) {
            $('#message').text("Error: " + args.get_message());
        }

    </script>
</head>
<body>
      <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    <p>
        <img id="profileImage" src=""/>
    </p>
        <p id="status"></p>
</body>
</html>
