﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />

<html>
<head>
    <title></title>
    <style type="text/css">
        .ms-backgroundImage {
            background-blend-mode: hard-light;
        }

        body {
            display: none;
        }
    </style>
    <!--[if IE]>
        <style type="text/css">
        .ms-backgroundImage:before{
           
        }
        body{
            display:none;
        }
        
    </style>
        <![endif]-->

    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>

    <SharePoint:ScriptLink ID="ScriptLink1" Name="init.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink2" Name="sp.init.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink3" Name="sp.runtime.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink4" Name="sp.core.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink5" Name="sp.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink6" Name="sp.ui.dialog.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>

    <script type="text/javascript">
        // Set the style of the client web part page to be consistent with the host web.

        'use strict';
        var type = '';
        var hostUrl ='';
        var appUrl = '';
        var currentUser;
        (function(){
            hostUrl = GetUrlKeyValue('SPHostUrl');
            appUrl = GetUrlKeyValue('SPAppWebUrl');
            var link = document.createElement('link');
            link.setAttribute('href', hostUrl + '/_layouts/15/defaultcss.ashx');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', '/_layouts/15/1033/styles/themable/corev15.css');
            document.head.appendChild(link);
        })();
        var scriptbase = hostUrl + "/_layouts/15/";
        $.getScript(scriptbase + "MicrosoftAjax.js",
            function () {
                $.getScript(scriptbase + "SP.js",
                    function () {
                        $.getScript(scriptbase + "SP.RequestExecutor.js", initContext);
                    });
            });

        var context;
        function initContext() {
            $(document).ready(function () {
                context = SP.ClientContext.get_current();
                var web = context.get_web();
                currentUser = context.get_web().get_currentUser();
                context.load(currentUser);
                context.executeQueryAsync(function () {
                    type = getQueryStringParameter("feedbacktype");
                    console.log(type);
                    var ctyeId = "";
                    var ctypeSuggetion = "0x01006AB561FBA7CF49CA8D1244D494307906003A33F0A456B74230B26275AFA6EBB9560044BFF14437217740A93ED5DD3AB0B024";
                    var ctypeIssueReport = "0x01006AB561FBA7CF49CA8D1244D49430790600FA1EA00FADAD4EAF95A4FC565FEE7B380079EEEFB568FDAA45B91E8F6C4972A348";
                    if (type == "1") {
                        $('#feedbackTitle').html("Suggestion Box");
                        ctyeId = ctypeSuggetion;
                    }
                    else {
                        $('#feedbackTitle').html("Issue Tracker");
                        ctyeId = ctypeIssueReport;
                    }
                    fillUserStats();
                    $("#inputButton input").click(function () {
                        var requestUrl = appUrl + '/Lists/FeedbackTrackerList/NewForm.aspx?ContentTypeID=' + ctyeId;
                        console.log(requestUrl);
                        var options = {
                            url: requestUrl,
                            autoSize: true,
                            title: "New Item",
                            dialogReturnValueCallback: fillUserStats
                        };
                        try {
                            SP.UI.ModalDialog.showModalDialog(options);
                        } catch (e) {
                            SP.SOD.execute('sp.ui.dialog.js', 'SP.UI.ModalDialog.showModalDialog', options);
                        }
                    });
                }, fillUserStatsFailed)
            });
        }

        function getQueryStringParameter(paramToRetrieve) {
            var params =
            document.URL.split("?")[1].split("&");
            var strParams = "";
            for (var i = 0; i < params.length; i = i + 1) {
                var singleParam = params[i].split("=");
                if (singleParam[0] == paramToRetrieve)
                    return singleParam[1];
            }
        }

        function fillUserStats() {
            var appweb = context.get_web();
            var feedbackList = appweb.get_lists().getByTitle("FeedbackTrackerList");
            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml('<View><Query><Where><Eq><FieldRef Name=\'Author\' LookupId=\'TRUE\'/><Value Type=\'Integer\'>' + currentUser.get_id() + '</Value></Eq></Where></Query></View>');
            var items = feedbackList.getItems(camlQuery);
            context.load(items);
            context.executeQueryAsync(function () {
                $('#userStats').html("Items submitted by you:" + items.get_count());
                $('body').css('display', 'block');
            }, fillUserStatsFailed);
        }
        function fillUserStatsFailed(sender, args) {
            console.log(args.get_message());
            $('#userStats').html("Failed to load user data");
            $('body').css('display', 'block');
        }
    </script>
</head>
<body>
    <h2 id="feedbackTitle"></h2>
    <div id="inputButton" style="padding: 20px;">
        <input type="button" value="Submit new" />
    </div>
    <div id="userStats">
    </div>
</body>
</html>
