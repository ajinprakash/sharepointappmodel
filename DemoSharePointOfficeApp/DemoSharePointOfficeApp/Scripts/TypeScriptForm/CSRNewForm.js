/// <reference path="../typings/microsoft-ajax/microsoft.ajax.d.ts" />
/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
/// <reference path="../sp-ts-csr.ts" />
var _;
(function (_) {
    function init() {
        return CSR.override(10001)
            .setInitialValue('Title', 'Feedback @' + new Date().format('dd/MMM/yyyy hh:mm:ss'))
            .makeReadOnly('Title')
            .register();
    }
    SP.SOD.executeOrDelayUntilScriptLoaded(function () {
        SP.SOD.executeOrDelayUntilScriptLoaded(init, "sp-ts-csr.ts");
        SP.SOD.executeOrDelayUntilScriptLoaded(function () {
            RegisterModuleInit(SPClientTemplates.Utility.ReplaceUrlTokens("~sites/Scripts/TypeScriptForm/CSRNewForm.js"), init);
        }, "sp.js");
    }, "clienttemplates.js");
})(_ || (_ = {}));
//# sourceMappingURL=CSRNewForm.js.map