﻿'use strict';

ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");

function initializePage()
{
    var context = SP.ClientContext.get_current();
    var user = context.get_web().get_currentUser();

    // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
    $(document).ready(function () {
        try {
            getUserName();
            ensureLookupColumnFix(context);
            construtUrls(context);
        } catch (e) {
            console.log(e.message);
        }
       
      
    });

    function construtUrls(context) {
        var appweb = context.get_web();
        var feedbackList = appweb.get_lists().getByTitle("FeedbackTrackerList");
        context.load(feedbackList);
        context.load(appweb);
        context.executeQueryAsync(function () {
            var url = appweb.get_url() + "/_layouts/15/listform.aspx?PageType=8&RootFolder&ListId=" + feedbackList.get_id();
            $('#newFormHref').attr("href", url + "&Source=" + encodeURI(window.location.href));
            url = appweb.get_url() + "/_layouts/15/listedit.aspx?List=" + feedbackList.get_id();
            $('#settingsHref').attr("href", url + "&Source=" + encodeURI(window.location.href));
        })
    }

    function ensureLookupColumnFix(context) {
        var appweb = context.get_web();
        var areasList = appweb.get_lists().getByTitle("Areas");
        var feedbackList = appweb.get_lists().getByTitle("FeedbackTrackerList");
        var field = appweb.get_availableFields().getByInternalNameOrTitle("Area");
        var listField = feedbackList.get_fields().getByInternalNameOrTitle("Area");
        context.load(areasList);        
        context.load(feedbackList);
        feedbackList.update();
        var views = feedbackList.get_views();
        context.load(field);
        context.load(listField);
        context.executeQueryAsync(function () {
            var fieldLookup = context.castTo(field, SP.FieldLookup);
            console.log(fieldLookup.get_schemaXml());
            fieldLookup.set_lookupList(areasList.get_id());
            fieldLookup.set_lookupField("Title");
            fieldLookup.update(true, true);

            var fieldLookupList = context.castTo(listField, SP.FieldLookup);
            console.log(fieldLookupList.get_schemaXml());
            fieldLookupList.set_lookupList(areasList.get_id());
            fieldLookupList.set_lookupField("Title");
            fieldLookupList.update();
            feedbackList.update();
            context.executeQueryAsync(function () {
                console.log('Successfull');
            }, function (sender, args) {
                console.log(args.get_message() + '\n' + args.get_stackTrace());
            });
        })
    }
    // This function prepares, loads, and then executes a SharePoint query to get the current users information
    function getUserName() {
        context.load(user);
        context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
    }

    // This function is executed if the above call is successful
    // It replaces the contents of the 'message' element with the user name
    function onGetUserNameSuccess() {
        $('#message').text('Hello ' + user.get_title());
    }

    // This function is executed if the above call fails
    function onGetUserNameFail(sender, args) {
        alert('Failed to get user name. Error:' + args.get_message());
    }

   
}
