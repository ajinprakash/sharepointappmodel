﻿window.JSLinkDemo = window.JSLinkDemo || {};
JSLinkDemo.OverrideRendering = function () {
    function onPostRenderTemplate(ctx) {
        if (ctx.ListData.Row == null || ctx.ListData.length < 1) {
            alert("no data");
        }
    }
    function StatusFieldViewTemplate(ctx) {
        var fieldVal = ctx.CurrentItem.Status;
        if (fieldVal == "Completed") {
            return "<span style='backgorund-color:green'>" + fieldVal + "</span>";
        }
        if (fieldVal == "In Progress") {
            return "<span style='backgorund-color:yellow'>" + fieldVal + "</span>";
        }
    }

    function registerTemplate() {
        var overrideCtx = {};
        overrideCtx.Template = {};
        overrideCtx.Template.Fields = {
            "Status": {
                "View": StatusFieldViewTemplate
            }
        };
        overrideCtx.OnPostRender = onPostRenderTemplate;
        SPClientTemplate.TemplateManager.RegisterTemplateOverrides(overrideCtx);
    }

    registerTemplate();
}
RegisterModuleInit("~site/SiteAssets/JSLink/JSLinkDemo-ListView.js", JSLinkDemo.OverrideRendering)
JSLinkDemo.OverrideRendering();