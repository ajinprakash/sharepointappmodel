﻿
var context;
var hostWebUrl = GetUrlKeyValue('SPHostUrl');
var appWebUrl = GetUrlKeyValue('SPAppWebUrl');
var scriptbase = hostWebUrl + "/_layouts/15/";
$.getScript(scriptbase + "MicrosoftAjax.js",
    function () {
        $.getScript(scriptbase + "SP.js",
            function () {
                $.getScript(scriptbase + "SP.RequestExecutor.js", initContextCustomizations);
            });
    });

function initContextCustomizations() {
    $(document).ready(function () {
        context = SP.ClientContext.get_current();
        $('#homepageHref').attr('href', 'Default.aspx?SPHostUrl=' + hostWebUrl + "&SPAppWebUrl=" + appWebUrl);
        $('#applyCustomization').click(function () {
            console.log("Starting....");
            hostWebUrl = GetUrlKeyValue('SPHostUrl');
            appWebUrl = GetUrlKeyValue('SPAppWebUrl');
            var appContextSite = new SP.AppContextSite(context, hostWebUrl);
            var hostWeb = appContextSite.get_web();
            addJsFile(context, hostWeb, appWebUrl).done(function (msg) {
                console.log(msg);
            });
            return false;
        });

        $('#disableCustomization').click(function () {
            console.log("Starting....");
             hostWebUrl = GetUrlKeyValue('SPHostUrl');
             appWebUrl = GetUrlKeyValue('SPAppWebUrl');
            var appContextSite = new SP.AppContextSite(context, hostWebUrl);
            var hostWeb = appContextSite.get_web();
            removeJsFile(context, hostWeb, appWebUrl).done(function (msg) {
                console.log(msg);
            });
            return false;
        });

    });

}


function addJsFile(ctx, hostWeb, appWebUrl) {
    var defered = $.Deferred();
    var revision = Date.now();
    var jslink = String.format('{0}/scripts/hostInjection.js?rev={1}', appWebUrl, revision);
    var scriptBlock = "\
        var demoFeedbackAppWebUrl='"+ appWebUrl + "';\
        var headID=document.getElementsByTagName('head')[0];\
        var newScript=document.createElement('script');\
        newScript.type='text/javascript';\
        newScript.src='"+ jslink + "';\
        headID.appendChild(newScript);";
    var existingActions = hostWeb.get_userCustomActions();
    ctx.load(existingActions);
    ctx.executeQueryAsync(function () {
        for (var i = 0; i < existingActions.get_count() ; i++) {
            var action = existingActions.getItemAtIndex(i);
            if (action.get_description() == "feedbackCustomization" && action.get_location() == "ScriptLink") {
                action.deleteObject();
            }
        }
        ctx.executeQueryAsync(function () {
            var newAction = existingActions.add();
            newAction.set_description("feedbackCustomization");
            newAction.set_location("ScriptLink");
            newAction.set_scriptBlock(scriptBlock);
            newAction.update();
            ctx.load(newAction);
            ctx.executeQueryAsync(function () {
                defered.resolve("Added Successfully!");
            }, function (s, a) {
                console.log(a.get_message());
                defered.resolve(a.get_message());
            });
        }, function (s, a) {
            console.log(a.get_message());
            defered.resolve(a.get_message());
        });
    }, function (s, a) {
        console.log(a.get_message());
        defered.resolve(a.get_message());
    });
    return defered.promise();
}

function removeJsFile(ctx, hostWeb, appWebUrl) {
    var defered = $.Deferred();        
    var existingActions = hostWeb.get_userCustomActions();
    ctx.load(existingActions);
    ctx.executeQueryAsync(function () {
        for (var i = 0; i < existingActions.get_count() ; i++) {
            var action = existingActions.getItemAtIndex(i);
            if (action.get_description() == "feedbackCustomization" && action.get_location() == "ScriptLink") {
                action.deleteObject();
            }
        }
        ctx.executeQueryAsync(function () {
            defered.resolve("Removed Successfully!");
        }, function (s, a) {
            console.log(a.get_message());
            defered.resolve(a.get_message());
        });
    }, function (s, a) {
        console.log(a.get_message());
        defered.resolve(a.get_message());
    });
}

function getQueryStringParameter(paramToRetrieve) {
    var params =
    document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve)
            return singleParam[1];
    }
}