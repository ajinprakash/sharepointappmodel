﻿demoFeedbackAppWebUrl = demoFeedbackAppWebUrl.toLowerCase();
console.log("Here we are at:" + window.location.href);
var headID
var newScript
if (!window.jQuery) {
    var headID = document.getElementsByTagName('head')[0];
    var newScript = document.createElement('script');
    newScript.type = 'text/javascript';
    newScript.src = demoFeedbackAppWebUrl + '/scripts/jquery-1.9.1.js';
    headID.appendChild(newScript);
    console.log("headerupdated");
}
newScript = document.createElement('link');
newScript.type = 'text/css';
newScript.rel = 'stylesheet';
newScript.href = demoFeedbackAppWebUrl + '/content/Feedback.css';
headID.appendChild(newScript);
defer();


function defer() {
    if (window.jQuery) {
        $.getScript("/_layouts/15/SP.js",
            function () {
                $.getScript(demoFeedbackAppWebUrl + "/_layouts/15/SP.RequestExecutor.js", function () {
                    $.getScript(demoFeedbackAppWebUrl + "/_layouts/15/init.js", mainLogic);
                });
            });
    }
    else {
        setTimeout(function () {
            defer();
        }, 100)
    }
}

var executor;
var digestValue;
function mainLogic() {
    loadFeedbackControlHtml();
    $("#feedback-tab").click(function () {
        $("#feedback-form").toggle("slide");
    });
    $("#feedback-form form").on('submit', onFeedbackFormSubmitClick)
    executor = new SP.RequestExecutor(demoFeedbackAppWebUrl.toLowerCase());
    getAppWebFormDigest().done(function (digestValue) {
        executor.executeAsync({
            url: (demoFeedbackAppWebUrl + "/_api/web/lists/getByTitle('Areas')/items").toLowerCase(),
            method: "GET",
            headers: {
                "Accept": "application/json,odata=verbose",
                "Content-Type": "application/json,odata=verbose",
                "X-RequestDigest": digestValue
            },
            success: onGetAreaItemSuccess,
            error: onRequestExecutorError
        });
    });
}



function loadFeedbackControlHtml() {
    var feedbackHtmlContainer = document.createElement("div");
    $(feedbackHtmlContainer).attr('id', 'feedbackMailDiv');
    $(feedbackHtmlContainer).html('<div id="feedback">\
        <div id="feedback-tab">Feedback</div>\
        <div id="feedback-form" style=\'display:none;\' class="col-xs-4 col-md-4 panel panel-default">\
        <form method="POST" class="form panel-body" role="form">\
        <div class="form-group"><input class="form-control" id="title" required autofocus name="title" placeholder="Title of feedback" type="text"/>\
        </div>\
        <div class="form-group">\
        Area:\
        <select class="select form-control" id="feedbackAreaSelect" class="select" name="area" required></select>\
        </div>\
        <div class="form-group">\
        <textarea class="form-control" id="body" name="body" placeholder="Please write your feedback here.." rows="5"></textarea>\
        </div>\
        <button class="btn btn-primary " name="submit" type="submit">Send</button>\
        </form>\
        </div>\
        </div>');
    $("#contentRow").prepend(feedbackHtmlContainer);
}

function getAppWebFormDigest() {
    var afterGettingDigest = $.Deferred();
    executor.executeAsync({
        url: demoFeedbackAppWebUrl + "/_api/contextinfo",
        method: "POST",
        headers: {
            "Accept": "application/json",
            "content-Type": "application/json"            
        },
        success: function (data) {
            if (data.statusCode == 200) {
                data = JSON.parse(data.body);
                if (data != null && data.d != null) { data = data.d; }
                if (data.GetContextWebInformation) {
                    afterGettingDigest.resolve(data.GetContextWebInformation.FormDigestValue);
                } else {
                    afterGettingDigest.resolve(data.FormDigestValue);
                }
            }
        },
        error: function (data, errorCode, errorMessage) {
            onRequestExecutorError(data, errorCode, errorMessage);
            afterGettingDigest.reject(errorMessage);
                
        }
    });
    return afterGettingDigest.promise();
}

function onGetAreaItemSuccess(data) {
    if (data.statusCode==200) {
        data = JSON.parse(data.body);
        if (data != null && data.d != null) {
            data = data.d;
            for (var i = 0; i < data.results.length; i++) {
                $('#feedbackAreaSelect').append($('<options>',{value:data.results[i].ID}).text(data.results[i].Title));
            }
        }
    }
}

function onFeedbackFormSubmitClick(event) {
    event.preventDefault();
    getAppWebFormDigest().done(function (digestValue) {
        var title = $("#feedback-form").find("input[type='text'][name='title']").val();
        var message = $("#feedback-form").find("textarea[name='message']").val();
        var areaID = $("#feedbackAreaSelect").val();
        var itemData = {
            _metadata: { type: "SP.Data.FeedbackTrackerListListItem" },
            "Title": title,
            "Message": message,
            AreaID: 1
        };
        executor.executeAsync({
            url: (demoFeedbackAppWebUrl + "/_api/web/lists/getByTitle('FeedbackTrackerList')/items").toLowerCase(),
            method: "POST",
            headers: {
                "Accept": "application/json,odata=verbose",
                "Content-Type": "application/json,odata=verbose",
                "X-RequestDigest":digestValue
            },
            body:JSON.stringify(itemData),
            success: function (data) {
                $("#feedback-form").find("input[type='text'][name='title']").val() = '';
                $("#feedback-form").find("textarea[name='message']").val() = '';
                $("#feedback-form").toggle("slide");
            },
            error: onRequestExecutorError

        });
    });
}
    
function onRequestExecutorError(data, errorCode, errorMessage) {
    console.log(errorMessage);
}



