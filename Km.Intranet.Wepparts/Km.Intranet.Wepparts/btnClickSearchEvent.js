﻿'use strict';
ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");

function initializePage() {
    var siteURL;
    var name;
    var staffno;
    var itemsArray = [];
    // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
    $(document).ready(function () {
       
        $("#btnSerach").bind("click", function () {
            GetEmployeeDataFromList();
        });

    });

    
    //Retrieve list items from sharepoint using API
    var filter;
    function GetEmployeeDataFromList() {
        var scriptbase = _spPageContextInfo.webServerRelativeUrl + "/_layouts/15/";       
        $.when(CreateRestURLFilter()).then(function () {
            siteURL = _spPageContextInfo.siteAbsoluteUrl;
            // siteURL = "http://hqinternetdev16";
            console.log("from top nav - " + siteURL);
            var apiPath = siteURL + "/_api/lists/getbytitle('Employee Master Data')/items" + filter + "'";
            $.ajax({
                url: apiPath,
                headers: {
                    Accept: "application/json;odata=verbose"
                },
                async: false,
                success: function (data) {
                    var items; // Data will have user object
                    var results;
                    if (data != null) {
                        items = data.d;
                        if (items != null) {
                            results = items.results
                            for (var i = 0; i < results.length; i++) {
                                //itemsArray.push({
                                //    "Title": results[i].Title
                                //});
                                console.log(results[i].Title);
                                // $.when(createSearchResulthtml(results[i]).done());
                                return true;

                            }

                        }
                    }
                },
                error: function (data) {
                    console.log("An error occurred. Please try again.");
                    return false;
                }
            });
        })
        
    }

    function CreateRestURLFilter() {
        try {
            var deferred = $.Deferred();
            var emplEngName = $('.engName').val();
            var emplArbName = $('.arbName').val();
            var emplStaffNo = $('.staffNo').val();
            var emplDirecorate = $('.directorate').find(":selected").text();
            var emplDepartment = $('.department').find(":selected").text();
            var emplSection = $('.section').find(":selected").text();
            var emplStatus = $('.status').find(":selected").text();
            var emplExEmployee = $('.exEmployee').attr('checked');
            var RestURL = "/_api/lists/getbytitle('Employee Master Data')/items";
            filter = "?$filter="; var prevAdded = false;
            if (emplEngName != "") {
                filter += "ENGLISHNAME eq " + emplEngName;
                prevAdded = true;
            }
            else if (emplArbName != "") {
                if (prevAdded) {
                    filter += "and ARABICNAME eq " + emplArbName;
                }
                else {
                    filter += "ARABICNAME eq " + emplArbName;
                    prevAdded = true;
                }
            }
            else if (staffno != "") {
                if (prevAdded) {
                    filter += "and Title eq " + staffno;
                }
                else {
                    filter += "Title eq " + staffno;
                    prevAdded = true;
                }
            }
            else if (emplDirecorate != "") {
                if (prevAdded) {
                    filter += "and DIRECTORATEID eq " + emplDirecorate;
                }
                else {
                    filter += "DIRECTORATEID eq " + emplDirecorate;
                    prevAdded = true;
                }
            }
            else if (emplDepartment != "") {
                if (prevAdded) {
                    filter += "and DEPARTMENTID eq " + emplDepartment;
                }
                else {
                    filter += "DEPARTMENTID eq " + emplDepartment;
                    prevAdded = true;
                }
            }
            else if (emplSection != "") {
                if (prevAdded) {
                    filter += "and SECTIONID eq " + emplSection;
                }
                else {
                    filter += "SECTIONID eq " + emplSection;
                    prevAdded = true;
                }
            }
            else if (emplStatus != "") {
                if (prevAdded) {
                    filter += "and STATUS eq " + emplStatus;
                }
                else {
                    filter += "STATUS eq " + emplStatus;
                    prevAdded = true;
                }
            }
            else if (emplExEmployee ==false) {
                if (prevAdded) {
                    filter += "and STATUS eq Available";
                }
                else {
                    filter += "STATUS eq Available";
                    prevAdded = true;
                }
            }
            if (prevAdded) {
                deferred.resolved();
            }
            else {
                deferred.reject();
            }

            return deferred.promise();

        } catch (e) {

        }
    }
   

}
