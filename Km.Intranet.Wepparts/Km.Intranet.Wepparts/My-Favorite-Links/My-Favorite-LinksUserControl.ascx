﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="My-Favorite-LinksUserControl.ascx.cs" Inherits="Km.Intranet.Wepparts.My_Favorite_Links.My_Favorite_LinksUserControl" %>



<!-- Style-->
<link href="./_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/Contents/App.css" rel="stylesheet" />
<link rel="stylesheet" href="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/Contents/bootstrap.min.css" />
<link href="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/Contents/jquery-ui.css" rel="stylesheet" />
<link href="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/Contents/style.css" rel="stylesheet" />
<!--End Style-->

<!--Scripts -->
<script src="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/scripts/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>
<script src="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/scripts/jquery-ui.js"></script>
<script src="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/scripts/bootstrap.min.js"></script>
<SharePoint:ScriptLink ID="ScriptLink1" Name="init.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
<SharePoint:ScriptLink ID="ScriptLink2" Name="sp.init.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
<SharePoint:ScriptLink ID="ScriptLink3" Name="sp.runtime.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
<SharePoint:ScriptLink ID="ScriptLink4" Name="sp.core.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
<SharePoint:ScriptLink ID="ScriptLink5" Name="sp.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
<SharePoint:ScriptLink ID="ScriptLink6" Name="SP.UserProfiles.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
<script src="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/scripts/TypeScriptFavoriteLinks/crudOperationsScript.js"></script>
<script src="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/scripts/TypeScriptFavoriteLinks/myFavoriteLinks.js"></script>
<!--End Scripts-->
 <script type="text/javascript">
        // enabling the sortable feature in list

        
        $(document).ready(function () {
            $(function () {
                $("#MyFavouriteLinks").sortable();
                $("#MyFavouriteLinks").disableSelection();
            });

        });
    </script>

<div class="col-xs-12">
        <div class="row">
            <div class="row">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">
                            My Favorite Links
                             <img id="toFavItemCreate" src="/_layouts/15/Kahramaa.Intranet.Webparts/Favorite-Links/Images/sign-add-icon.png" alt="Add New Item" class="fav_btn pull-right" />
                        </div>
                       
                    </div>
                    <div class="card-body">
                        <ul id="MyFavouriteLinks" class="ui-sortable connectedSortable">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myFavItemModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Favorite Link</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <textarea rows="4" id="txtTitle" class="title" placeholder="Enter Title Here.."></textarea>
                    </p>

                    <p>
                        <textarea rows="4" id="txtURL" class="title" placeholder="Enter Link Here.."></textarea>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnSubmitFavLinkItem" data-dismiss="modal">Submit</button>
                </div>
            </div>

        </div>
    </div>