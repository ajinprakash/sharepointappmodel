﻿'use strict';
ExecuteOrDelayUntilScriptLoaded(initializePage, "sp.js");

function initializePage() {
    var siteURL;
    var name;
    var staffno;
    var itemsArray = [];
    // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
    $(document).ready(function () {      
        var scriptbase = _spPageContextInfo.webServerRelativeUrl + "/_layouts/15/";
        //var scriptbase =  "http://hqinternetdev16/_layouts/15/";
        var permanentEmployee = GetEmployeeDataFromList();
        if (permanentEmployee != true) {
            GetEmployeeDataFromUserProfile()
        }
        else {

        }

    });
    //Retrieve list items from sharepoint using API
    function GetEmployeeDataFromList() {
        var urlParams = new URLSearchParams(window.location.search);       
        if (urlParams.has('staffno')) {
            staffno = urlParams.get('staffno');
        }
        siteURL = _spPageContextInfo.siteAbsoluteUrl;
        // siteURL = "http://hqinternetdev16";
        console.log("from top nav - " + siteURL);
        var apiPath = siteURL + "/_api/lists/getbytitle('Employee Master Data')/items?$filter=Title eq '" + staffno + "'and STATUS eq Available";
        $.ajax({
            url: apiPath,
            headers: {
                Accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (data) {
                var items; // Data will have user object
                var results;
                if (data != null) {
                    items = data.d;
                    if (items != null) {
                        results = items.results
                        for (var i = 0; i < results.length; i++) {
                            //itemsArray.push({
                            //    "Title": results[i].Title
                            //});
                            console.log(results[i].Title);
                            // $.when(createSearchResulthtml(results[i]).done());
                            return true;

                        }

                    }
                }
            },
            error: function (data) {
                console.log("An error occurred. Please try again.");
                return false;
            }
        });
    }
    function GetEmployeeDataFromUserProfile() {
        userProfileServiceLoad();
    }

}


var UserProfileFetch = /** @class */ (function () {
    function UserProfileFetch() {        
        this._currentContext = SP.ClientContext.get_current();       
        this._web = this._hostContext.get_web();
    }    
    return UserProfileFetch;
}());

function userProfileServiceLoad() {
    ExecuteOrDelayUntilScriptLoaded(fetchPrfileData, "sp.userprofiles.js");
}
function fetchPrfileData() {
    var foo = new UserProfileFetch();
    var peopleManager = new SP.UserProfiles.PeopleManager(foo._currentContext);
    var personProperties = peopleManager.getPropertiesFor(staffno);
    foo._currentContext.load(personProperties);
    foo._currentContext.executeQueryAsync(function (sender, args) {
        var properties = personProperties.get_userProfileProperties();
        var messageText = "";
        for (var key in properties) {
            messageText += "<br />[" + key + "]: \"" + properties[key] + "\"";
            if (key == "PictureURL") {
                $get("PictureURL").setAttribute('src', properties[key].toString().replace("MThumb", "LThumb"));
            }
            if (key == "PreferredName") {
                $get("PreferredName").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "Title") {
                $get("Title").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "Department") {
                $get("Department").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "WorkPhone") {
                $get("WorkPhone").innerText = properties[key].toString().toUpperCase();
            }
            if (key == "WorkEmail") {
                $get("WorkEmail").innerText = properties[key].toString();
                $get("emailLink").setAttribute('href', "mailto:" + properties[key].toString());
            }
            if (key == "PersonalSpace") {
                $get("profileButton").setAttribute('href', properties[key].toString());
            }
            if (key == "Manager") {
                getManagerData(properties[key].toString(), peopleManager, foo._currentContext);
            }
        }
        $get("alldata").innerText = messageText;
        // $('.alldata').hide();
    }, function (sender, args) { alert('Error: ' + args.get_message()); });
}
function getManagerData(targetUser, peopleManager, context) {
    var deferred = $.Deferred();
    var personProperties = peopleManager.getPropertiesFor(targetUser);
    // Load the PersonProperties object and send the request.
    context.load(personProperties);
    context.executeQueryAsync(function () {
        var properties = personProperties.get_userProfileProperties();
        for (var key in properties) {
            if (key == "PreferredName") {
                $get("Manager").innerText = properties[key].toString().toUpperCase();
                deferred.resolve();
            }
        }
    }, function (sender, args) { alert('Error: ' + args.get_message()); deferred.reject(); });
    return deferred.promise();
}
//#





