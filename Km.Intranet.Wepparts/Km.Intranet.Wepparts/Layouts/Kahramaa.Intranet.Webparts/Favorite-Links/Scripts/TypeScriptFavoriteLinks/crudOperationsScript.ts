﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />

class BasicOperationsFavLink implements ICrudFetchOperation, ICrudDeleteOperation, ICrudCurrentUserOperation, ILogger {
    //Define properties with respect BasicOperations Class  
    _currentContext: SP.ClientContext;
    _hostContext: SP.AppContextSite;
    _web: SP.Web;
    _colList: SP.ListCollection;
    _list: SP.List;
    _listName: string;
    _listItems: SP.ListItemCollection;
    _newItem: SP.ListItem;
    query: SP.CamlQuery;
    _currentuser: SP.User;
    _listItemEnumerator: IEnumerable<SP.ListItem>;
    _listObjectEnumerator: IEnumerable<object>;
    _queryString: string;
    //base constructor
    constructor() {
        let hostweburl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._currentContext = SP.ClientContext.get_current();
        this._hostContext = new SP.AppContextSite(this._currentContext, hostweburl);
        let appweburl = decodeURIComponent(this.getQueryStringParameter("SPAppWebUrl"));
        this._web = this._hostContext.get_web();
        this.GetCurrentUser();
    }

    //memeber method to get current user
    GetCurrentUser(): any {
        var defere = $.Deferred();
        this._currentuser = this._currentContext.get_web().get_currentUser();
        this._currentContext.load(this._currentuser);
        this._currentContext.executeQueryAsync(function () {
            // console.log(this._currentuser.get_loginName());             
            defere.resolve();
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            defere.reject();
        });
        return defere.promise();
    }

    //member method to get query string parameters
    private getQueryStringParameter(urlParameterKey: string) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] == urlParameterKey)
                return decodeURIComponent(singleParam[1]);
        }
    }
    //member method to get all list items
    GetAllItems(listname): void {
        var defere = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                defere.resolve();
            }
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            defere.reject();
        });
        defere.promise()
    }
    //member method to get list items with list name, query
    GetAllItemsWithQuery(listname, query): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml(query);
        var listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(listItems);
        this._currentContext.executeQueryAsync(function () {
            if (listItems.get_count() > 0) {
                var listItemEnumerator = listItems.getEnumerator();
                if (listItemEnumerator != null) {
                    while (listItemEnumerator.moveNext()) {
                        var currentItem = listItemEnumerator.get_current();
                        $("#MyFavouriteLinks").append("<li class='ui-state-default ui-sortable-handle' id='"+currentItem.get_id()+"' data-order='" + currentItem.get_item('URLOrder') + "'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span><a class='fav-link-anchor'  href='" + currentItem.get_item('URL') + "'>" + currentItem.get_item('Title') + "</a><img src='../Images/remove-icon-png.png' class='fav_btn del_Img pull-right'></li>");
                    }
                }
                deferred.resolve();
            }
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    }
    //member method to get list items with list name & view name
    GetAllItemsFromView(listname, viewname): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var viewQuery = this._list.get_views().getByTitle(viewname).get_viewQuery();
        this.query = new SP.CamlQuery();
        this.query.set_viewXml('<View><Query>' + viewQuery + '</View></Query>');
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                deferred.resolve();
            }
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    }
    //member method to get list item with list name & id
    Getitem(listname, id): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(function () {
            if (this._newItem != null) {
                deferred.resolve();
            }
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    }
    //member method to update all list item with list name , id & json values
    UpdateItem(listname, id, jsonValues): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var new_Item = this._list.getItemById(id);
        var data = jsonValues;
        new_Item.set_item("URLOrder", data.URLOrder.toString());
        new_Item.update();
        this._currentContext.executeQueryAsync(function () {
            console.log("item updated");
            deferred.resolve();
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();

    }
    //member method to delete all list item with list name
    DeleteAllItems(listname): void {
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(() => this.OnSuccessDeleteAll(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessDeleteAll() {
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                this._currentContext.executeQueryAsync(() => this.OnSuccessDeleteSingleItem(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
            }
        }
        else { console.log("no item to delete!!") }
    }
    private OnSuccessDeleteSingleItem() {
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            this._currentContext.executeQueryAsync(() => this.DeleteAllItems(this._listName), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
        }
    }
    //member method to delete all list item with list name
    DeleteAllItemsWithQuery(listname, queryString): void {
        this._listName = listname;
        this._queryString = queryString;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml(this._queryString);
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(() => this.OnSuccessDeleteAllWithQuery(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
    }
    private OnSuccessDeleteAllWithQuery() {
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                this._currentContext.executeQueryAsync(() => this.OnSuccessDeleteSingleItemWithQuery(), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
            }
        }
        else { console.log("no tiem to delete!!") }
    }
    private OnSuccessDeleteSingleItemWithQuery() {
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            this._currentContext.executeQueryAsync(() => this.DeleteAllItemsWithQuery(this._listName, this._queryString), (sender: any, args: SP.ClientRequestFailedEventArgs) => this.logError(this, args));
        }
    }
    //member method to delete list item with list name & id
    DeleteItem(listname, id): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this._newItem = this._list.getItemById(parseInt(id));
        this._newItem.deleteObject();
        this._currentContext.executeQueryAsync(function () {
            console.log(id);
            deferred.resolve();
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();           
        });
        return deferred.promise();
    }


    DeleteItem_updateDom(listname, domCurrentElement): any {
        var deferred = $.Deferred();
        var id = $(domCurrentElement).parent().attr('id');
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this._newItem = this._list.getItemById(parseInt(id));
        this._newItem.deleteObject();
        this._currentContext.executeQueryAsync(function () {
            console.log(id);
            $(domCurrentElement).parent().remove();
            deferred.resolve();   
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    }
    //member method to add list item with list name and json values
    AddItem(listname, jsonValues): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        var data = JSON.parse(jsonValues);
        $.each(data, function (index, element) {
            this._newItem.set_item(data[index].key, data[index].value);
        });
        newListItem.update();
        this._currentContext.load(newListItem);
        this._currentContext.executeQueryAsync(function () {
            console.log(newListItem.get_item("ID"));
            deferred.resolve();
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    }

    AddItem_DomUpdate(listname, jsonValues, domElementLink, domElementDelImage, domElementSpan, domParent): any {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        newListItem.set_item("Title", jsonValues.Title.toString());
        newListItem.set_item("URL", jsonValues.URL.toString());
        newListItem.set_item("URLOrder", $(domParent).children().length);
        newListItem.update();
        this._currentContext.executeQueryAsync(function () {
            console.log(newListItem.get_id());
            var domElementli = $("<li/>", {
                "data-order": newListItem.get_item("URLOrder"),
                "class": "ui-state-default ui-sortable-handle",
                "id": newListItem.get_id()
            });
            $(domElementli).append(domElementSpan);
            $(domElementli).append(domElementLink);
            $(domElementli).append(domElementDelImage);
            $(domParent).append(domElementli);
            deferred.resolve();
        }, function (sender: any, args: SP.ClientRequestFailedEventArgs) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    }
    //Member method to log errors
    logError(sender, args) {
        console.log("Request Failed: " + args.get_message());
    }

}

interface ICrudFetchOperation {
    GetAllItems(listname): any;
    GetAllItemsWithQuery(listname, query): any;
    Getitem(listname, id): any;
}

interface ICrudInserUpdatetOperation {
    AddItem(): any;
    AddItem_DomUpdate(listname, jsonValues, domElement, domParent): any;
    UpdateItem(listname, id, jsonValues): any;
}
interface ICrudDeleteOperation {
    DeleteItem(listname, id): any;
    DeleteAllItems(listname): void;
    DeleteItem_updateDom(listname, id, domParentElement, domCurrentElement): any;
}
interface ICrudCurrentUserOperation {
    GetCurrentUser(): any;
}

interface ILogger {
    logError(sender: any, e: SP.ClientRequestFailedEventArgs): void;
}






