/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
var BasicOperationsFavLink = /** @class */ (function () {
    //base constructor
    function BasicOperationsFavLink() {
        var hostweburl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._currentContext = SP.ClientContext.get_current();
        this._hostContext = new SP.AppContextSite(this._currentContext, hostweburl);
        var appweburl = decodeURIComponent(this.getQueryStringParameter("SPAppWebUrl"));
        this._web = this._hostContext.get_web();
        this.GetCurrentUser();
    }
    //memeber method to get current user
    BasicOperationsFavLink.prototype.GetCurrentUser = function () {
        var defere = $.Deferred();
        this._currentuser = this._currentContext.get_web().get_currentUser();
        this._currentContext.load(this._currentuser);
        this._currentContext.executeQueryAsync(function () {
            // console.log(this._currentuser.get_loginName());             
            defere.resolve();
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            defere.reject();
        });
        return defere.promise();
    };
    //member method to get query string parameters
    BasicOperationsFavLink.prototype.getQueryStringParameter = function (urlParameterKey) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] == urlParameterKey)
                return decodeURIComponent(singleParam[1]);
        }
    };
    //member method to get all list items
    BasicOperationsFavLink.prototype.GetAllItems = function (listname) {
        var defere = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                defere.resolve();
            }
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            defere.reject();
        });
        defere.promise();
    };
    //member method to get list items with list name, query
    BasicOperationsFavLink.prototype.GetAllItemsWithQuery = function (listname, query) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml(query);
        var listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(listItems);
        this._currentContext.executeQueryAsync(function () {
            if (listItems.get_count() > 0) {
                var listItemEnumerator = listItems.getEnumerator();
                if (listItemEnumerator != null) {
                    while (listItemEnumerator.moveNext()) {
                        var currentItem = listItemEnumerator.get_current();
                        $("#MyFavouriteLinks").append("<li class='ui-state-default ui-sortable-handle' id='" + currentItem.get_id() + "' data-order='" + currentItem.get_item('URLOrder') + "'><span class='ui-icon ui-icon-arrowthick-2-n-s'></span><a class='fav-link-anchor'  href='" + currentItem.get_item('URL') + "'>" + currentItem.get_item('Title') + "</a><img src='../Images/remove-icon-png.png' class='fav_btn del_Img pull-right'></li>");
                    }
                }
                deferred.resolve();
            }
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    //member method to get list items with list name & view name
    BasicOperationsFavLink.prototype.GetAllItemsFromView = function (listname, viewname) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var viewQuery = this._list.get_views().getByTitle(viewname).get_viewQuery();
        this.query = new SP.CamlQuery();
        this.query.set_viewXml('<View><Query>' + viewQuery + '</View></Query>');
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                deferred.resolve();
            }
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    //member method to get list item with list name & id
    BasicOperationsFavLink.prototype.Getitem = function (listname, id) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(function () {
            if (this._newItem != null) {
                deferred.resolve();
            }
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    //member method to update all list item with list name , id & json values
    BasicOperationsFavLink.prototype.UpdateItem = function (listname, id, jsonValues) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var new_Item = this._list.getItemById(id);
        var data = jsonValues;
        new_Item.set_item("URLOrder", data.URLOrder.toString());
        new_Item.update();
        this._currentContext.executeQueryAsync(function () {
            console.log("item updated");
            deferred.resolve();
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    //member method to delete all list item with list name
    BasicOperationsFavLink.prototype.DeleteAllItems = function (listname) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteAll(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperationsFavLink.prototype.OnSuccessDeleteAll = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteSingleItem(); }, function (sender, args) { return _this.logError(_this, args); });
            }
        }
        else {
            console.log("no item to delete!!");
        }
    };
    BasicOperationsFavLink.prototype.OnSuccessDeleteSingleItem = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            this._currentContext.executeQueryAsync(function () { return _this.DeleteAllItems(_this._listName); }, function (sender, args) { return _this.logError(_this, args); });
        }
    };
    //member method to delete all list item with list name
    BasicOperationsFavLink.prototype.DeleteAllItemsWithQuery = function (listname, queryString) {
        var _this = this;
        this._listName = listname;
        this._queryString = queryString;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml(this._queryString);
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteAllWithQuery(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperationsFavLink.prototype.OnSuccessDeleteAllWithQuery = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteSingleItemWithQuery(); }, function (sender, args) { return _this.logError(_this, args); });
            }
        }
        else {
            console.log("no tiem to delete!!");
        }
    };
    BasicOperationsFavLink.prototype.OnSuccessDeleteSingleItemWithQuery = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            this._currentContext.executeQueryAsync(function () { return _this.DeleteAllItemsWithQuery(_this._listName, _this._queryString); }, function (sender, args) { return _this.logError(_this, args); });
        }
    };
    //member method to delete list item with list name & id
    BasicOperationsFavLink.prototype.DeleteItem = function (listname, id) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this._newItem = this._list.getItemById(parseInt(id));
        this._newItem.deleteObject();
        this._currentContext.executeQueryAsync(function () {
            console.log(id);
            deferred.resolve();
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    BasicOperationsFavLink.prototype.DeleteItem_updateDom = function (listname, domCurrentElement) {
        var deferred = $.Deferred();
        var id = $(domCurrentElement).parent().attr('id');
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this._newItem = this._list.getItemById(parseInt(id));
        this._newItem.deleteObject();
        this._currentContext.executeQueryAsync(function () {
            console.log(id);
            $(domCurrentElement).parent().remove();
            deferred.resolve();
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    //member method to add list item with list name and json values
    BasicOperationsFavLink.prototype.AddItem = function (listname, jsonValues) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        var data = JSON.parse(jsonValues);
        $.each(data, function (index, element) {
            this._newItem.set_item(data[index].key, data[index].value);
        });
        newListItem.update();
        this._currentContext.load(newListItem);
        this._currentContext.executeQueryAsync(function () {
            console.log(newListItem.get_item("ID"));
            deferred.resolve();
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    BasicOperationsFavLink.prototype.AddItem_DomUpdate = function (listname, jsonValues, domElementLink, domElementDelImage, domElementSpan, domParent) {
        var deferred = $.Deferred();
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        newListItem.set_item("Title", jsonValues.Title.toString());
        newListItem.set_item("URL", jsonValues.URL.toString());
        newListItem.set_item("URLOrder", $(domParent).children().length);
        newListItem.update();
        this._currentContext.executeQueryAsync(function () {
            console.log(newListItem.get_id());
            var domElementli = $("<li/>", {
                "data-order": newListItem.get_item("URLOrder"),
                "class": "ui-state-default ui-sortable-handle",
                "id": newListItem.get_id()
            });
            $(domElementli).append(domElementSpan);
            $(domElementli).append(domElementLink);
            $(domElementli).append(domElementDelImage);
            $(domParent).append(domElementli);
            deferred.resolve();
        }, function (sender, args) {
            console.log(args.get_errorDetails());
            deferred.reject();
        });
        return deferred.promise();
    };
    //Member method to log errors
    BasicOperationsFavLink.prototype.logError = function (sender, args) {
        console.log("Request Failed: " + args.get_message());
    };
    return BasicOperationsFavLink;
}());
//# sourceMappingURL=crudOperationsScript.js.map