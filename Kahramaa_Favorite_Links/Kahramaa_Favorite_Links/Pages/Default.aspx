﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-3.3.1.js"></script>
    <meta name="WebPartPageExpansion" content="full" />
    <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <SharePoint:ScriptLink ID="ScriptLink5" Name="sp.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <SharePoint:ScriptLink ID="ScriptLink6" Name="SP.UserProfiles.js" runat="server" OnDemand="false" LoadAfterUI="true" Localizable="false"></SharePoint:ScriptLink>
    <script type="text/javascript" src="../Scripts/TypeScriptFavoriteLinks/crudOperationsScript.js"></script>
    <script type="text/javascript" src="../Scripts/TypeScriptFavoriteLinks/myFavoriteLinks.js"></script>
    <link href="../Content/jquery-ui.css" rel="stylesheet" />
    <link href="../Content/style.css" rel="stylesheet" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="row">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">
                            My Favorite Links
                             <img id="toFavItemCreate" src="../Images/sign-add-icon.png" alt="Add New Item" class="fav_btn pull-right" />
                        </div>
                       
                    </div>
                    <div class="card-body">
                        <ul id="MyFavouriteLinks" class="ui-sortable connectedSortable">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myFavItemModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Favorite Link</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <textarea rows="4" id="txtTitle" class="title form-control" placeholder="Enter Title Here.."></textarea>
                    </p>

                    <p>
                        <textarea rows="4" id="txtURL" class="title form-control" placeholder="Enter Link Here.."></textarea>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btnSubmitFavLinkItem" data-dismiss="modal">Submit</button>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
