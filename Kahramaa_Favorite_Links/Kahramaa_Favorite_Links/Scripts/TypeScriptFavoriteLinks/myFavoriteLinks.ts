﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/jqueryui/jqueryui.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
/// <reference path="../typings/microsoft-ajax/microsoft.ajax.d.ts" />
/// <reference path="../typings/bootstrap/index.d.ts" />
/// <reference path="crudoperationsscript.ts" />


'use strict';
ExecuteOrDelayUntilScriptLoaded(pageLoadfavLink, "sp.js");
function pageLoadfavLink() {
    let foo: BasicOperationsFavLink = new BasicOperationsFavLink();
    $.when(foo.GetCurrentUser()).done(function () {
        var query = "<View><Query><Where><Eq><FieldRef Name='Author' /><Value Type='User'>" + foo._currentuser.get_title() + "</Value></Eq></Where><OrderBy><FieldRef Name='URLOrder' Ascending='True' /></OrderBy></Query></View>";
        var listName = "My Favorite Links";
        $.when(foo.GetAllItemsWithQuery(listName, query)).done(function () {
            $("#MyFavouriteLinks").sortable({
                connectWith: ".connectedSortable",
                start: function (event, ui) {
                    console.log("Start " + $(ui.item.html()).attr("data-order"));
                },
                stop: function (event, ui) {
                    console.log("End " + $(ui.item.html()).attr("data-order"));
                    reorderElemets($(ui.item).parent());
                    //call update order
                }

            }).disableSelection();

            $(".btnSubmitFavLinkItem").click(function () {
                var dataObj = { 'Title': $("#txtTitle").val(), 'URL': $("#txtURL").val() };
                var jsonString = JSON.stringify(dataObj)
                var dataJsonObj = JSON.parse(jsonString);
                var domElementLink = $("<a/>", {
                    href: $("#txtURL").val(),
                    text: $("#txtTitle").val(),
                    "class": "fav-link-anchor"
                });

                var domElementSpan = $("<span/>", {
                    text: $get("txtTitle").innerText,
                    "class": "ui-icon ui-icon-arrowthick-2-n-s"
                });
                var domElementDelImage = $("<img/>", {
                    src: "../Images/remove-icon-png.png",
                    alt: "del Image",
                    class: "fav_btn del_Img pull-right"                    
                });
                $(domElementDelImage).click(function (e) {
                    delItemMethod(e);
                });

                var domParent = $("#MyFavouriteLinks");
                $.when(foo.AddItem_DomUpdate("My Favorite Links", dataJsonObj, domElementLink, domElementDelImage, domElementSpan, domParent)).done(function () {
                    console.log("added");
                    $("#txtURL").val("");
                    $("#txtTitle").val("");
                });
            });

            $("#toFavItemCreate").click(function () {
                $("#myFavItemModal").modal();
            });

            var deleteElements = $(".del_Img");
            $(deleteElements).each(function (index, element) {
                $(deleteElements[index]).click(function (e) {
                    delItemMethod(e);
                });
            });
            function delItemMethod(e) {                
                var deletedElementParent = $(e.target).parent().parent();
                // $(e.target).parent().parent().css("display", "none");
                $.when(foo.DeleteItem_updateDom("My Favorite Links", $(e.target))).done(function () {
                    reorderElemets(deletedElementParent);
                });
            }

            function reorderElemets(elements) {
                if (elements != undefined) {
                    var i = 0;
                    $(elements[0].children).each(function (i, obj) {
                        var dataString = { 'URLOrder': i };
                        var jsonString = JSON.stringify(dataString)
                        var dataJsonObj = JSON.parse(jsonString);
                        $.when(foo.UpdateItem("My Favorite Links", parseInt(elements[0].children[i].id), dataJsonObj)).done(function () {
                            console.log("Order Updated");
                        });
                    });

                }
            }

        });
    });

}









