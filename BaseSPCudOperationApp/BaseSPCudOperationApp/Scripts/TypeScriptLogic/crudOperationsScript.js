/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/sharepoint/sharepoint.d.ts" />
var BasicOperations = /** @class */ (function () {
    //base constructor
    function BasicOperations() {
        var hostweburl = decodeURIComponent(this.getQueryStringParameter("SPHostUrl"));
        this._currentContext = SP.ClientContext.get_current();
        this._hostContext = new SP.AppContextSite(this._currentContext, hostweburl);
        var appweburl = decodeURIComponent(this.getQueryStringParameter("SPAppWebUrl"));
        this._web = this._hostContext.get_web();
    }
    //memeber method to get current user
    BasicOperations.prototype.GetCurrentUser = function () {
        var _this = this;
        this._currentuser = this._hostContext.get_web().get_currentUser();
        this._currentContext.load(this._currentuser);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessFullUserLoad(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessFullUserLoad = function () {
        console.log(this._currentuser.get_loginName());
        return this._currentuser;
    };
    //member method to get query string parameters
    BasicOperations.prototype.getQueryStringParameter = function (urlParameterKey) {
        var params = document.URL.split('?')[1].split('&');
        var strParams = '';
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split('=');
            if (singleParam[0] == urlParameterKey)
                return decodeURIComponent(singleParam[1]);
        }
    };
    //member method to get all list items
    BasicOperations.prototype.GetAllItems = function (listname) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                return this._listItemEnumerator;
            }
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //member method to get list items with list name, query
    BasicOperations.prototype.GetAllItemsWithQuery = function (listname, query) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml = query;
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                return this._listItemEnumerator;
            }
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //member method to get list items with list name & view name
    BasicOperations.prototype.GetAllItemsFromView = function (listname, viewname) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var viewQuery = this._list.get_views().getByTitle(viewname).get_viewQuery();
        this.query = new SP.CamlQuery();
        this.query.set_viewXml('<View><Query>' + viewQuery + '</View></Query>');
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return function () {
            if (this._listItems.get_count() > 0) {
                this._listItemEnumerator = this._listItems.getEnumerator();
                return this._listItemEnumerator;
            }
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //member method to get list item with list name & id
    BasicOperations.prototype.Getitem = function (listname, id) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        console.log(id);
        this._newItem = this._list.getItemById(id);
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(function () { return function () {
            if (this._newItem != null) {
                return this._newItem;
            }
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //member method to update all list item with list name , id & json values
    BasicOperations.prototype.UpdateItem = function (listname, id, jsonValues) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this._newItem = this._list.getItemById(id);
        var data = JSON.parse(jsonValues.d);
        $.each(data.dates, function (index, element) {
            alert(element.timeStamp);
        });
        this._newItem.update();
        this._currentContext.load(this._newItem);
        this._currentContext.executeQueryAsync(function () { return function () {
            return id;
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //member method to delete all list item with list name
    BasicOperations.prototype.DeleteAllItems = function (listname) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteAll(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessDeleteAll = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteSingleItem(); }, function (sender, args) { return _this.logError(_this, args); });
            }
        }
        else {
            console.log("no tiem to delete!!");
        }
    };
    BasicOperations.prototype.OnSuccessDeleteSingleItem = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            this._currentContext.executeQueryAsync(function () { return _this.DeleteAllItems(_this._listName); }, function (sender, args) { return _this.logError(_this, args); });
        }
    };
    //member method to delete all list item with list name
    BasicOperations.prototype.DeleteAllItemsWithQuery = function (listname, queryString) {
        var _this = this;
        this._listName = listname;
        this._queryString = queryString;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this.query = new SP.CamlQuery();
        this.query.set_viewXml(this._queryString);
        this._listItems = this._list.getItems(this.query);
        this._currentContext.load(this._list);
        this._currentContext.load(this._listItems);
        this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteAllWithQuery(); }, function (sender, args) { return _this.logError(_this, args); });
    };
    BasicOperations.prototype.OnSuccessDeleteAllWithQuery = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        var itemCount = this._listItems.get_count();
        if (itemCount > 0) {
            while (listItemEnumerator.moveNext()) {
                var currentItem = listItemEnumerator.get_current();
                currentItem.deleteObject();
                this._currentContext.executeQueryAsync(function () { return _this.OnSuccessDeleteSingleItem(); }, function (sender, args) { return _this.logError(_this, args); });
            }
        }
        else {
            console.log("no tiem to delete!!");
        }
    };
    BasicOperations.prototype.OnSuccessDeleteSingleItemWithQuery = function () {
        var _this = this;
        var listItemEnumerator = this._listItems.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var currentItem = listItemEnumerator.get_current();
            currentItem.deleteObject();
            this._currentContext.executeQueryAsync(function () { return _this.DeleteAllItemsWithQuery(_this._listName, _this._queryString); }, function (sender, args) { return _this.logError(_this, args); });
        }
    };
    //member method to delete list item with list name & id
    BasicOperations.prototype.DeleteItem = function (listname, id) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        this._newItem = this._list.getItemById(parseInt(id));
        this._newItem.deleteObject();
        this._currentContext.executeQueryAsync(function () { return function () {
            console.log(id);
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //member method to add list item with list name and json values
    BasicOperations.prototype.AddItem = function (listname, jsonValues) {
        var _this = this;
        this._listName = listname;
        this._list = this._web.get_lists().getByTitle(this._listName);
        var listItemInfo = new SP.ListItemCreationInformation();
        var newListItem = this._list.addItem(listItemInfo);
        var data = JSON.parse(jsonValues.d);
        $.each(data.dates, function (index, element) {
            alert(element.timeStamp);
        });
        newListItem.update();
        this._currentContext.load(newListItem);
        this._currentContext.executeQueryAsync(function () { return function () {
            console.log(newListItem.get_item("ID"));
        }; }, function (sender, args) { return _this.logError(_this, args); });
    };
    //Member method to log errors
    BasicOperations.prototype.logError = function (sender, args) {
        console.log("Request Failed: " + args.get_message());
    };
    return BasicOperations;
}());
//# sourceMappingURL=crudOperationsScript.js.map