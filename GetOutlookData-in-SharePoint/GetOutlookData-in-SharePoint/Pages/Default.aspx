﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <SharePoint:ScriptLink Name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet" />
     <link href="../Content/style.css" rel="stylesheet" />
    <link href="../Content/unitegallery/css/unite-gallery.css" rel="stylesheet" />
    <link href="../Content/themes/default/ug-theme-default.css" rel="stylesheet" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://kjur.github.io/jsrsasign/jsrsasign-latest-all-min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>
    <script type="text/javascript" src="../Scripts/graph-js-sdk-web.js"></script>
    <script type="text/javascript" src="../Scripts/outlook-demo.js"></script>

    <script type='text/javascript' src='../Scripts/unitegallery/ug-common-libraries.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-functions.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-thumbsgeneral.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-thumbsstrip.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-touchthumbs.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-panelsbase.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-strippanel.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-gridpanel.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-thumbsgrid.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-tiles.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-tiledesign.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-avia.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-slider.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-sliderassets.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-touchslider.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-zoomslider.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-video.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-gallery.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-lightbox.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-carousel.js'></script>
    <script type='text/javascript' src='../Scripts/unitegallery/ug-api.js'></script>
    <script type="text/javascript" src="../Content/themes/default/ug-theme-default.js"></script>

</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

  <!-- The following content will be replaced with the user name when you run the app - see App.js -->
    <div class="container main-container">
        <div id="signin-prompt" class="row calenderContainer">
            <div class="col-sm-12">
                <h1>Outlook Authorization</h1>
                <p>Kindly click the below button to sync you 0365 email account to SharePoint.</p>
                <p>
                    <a class="btn btn-lg btn-primary" href="#" role="button" id="connect-button">Connect to Outlook</a>
                </p>
            </div>
        </div>
        <!-- logged in user welcome -->
        <div id="logged-in-welcome" style="display: none;" class="row calenderContainer">
            <h1>Outlook SPA</h1>
            <p>Welcome <span id="username"></span>! Please use the nav menu to access your Outlook data.</p>
        </div>
        <!-- unsupported browser message -->
        <div id="unsupported" class="row page ">
            <div class="col-sm-12">
                <h1>Oops....</h1>
                <p>This page requires browser support for <a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API">session storage</a> and <a href="https://developer.mozilla.org/en-US/docs/Web/API/RandomSource/getRandomValues"><code>crypto.getRandomValues</code></a>. Unfortunately, your browser does not support one or both features. Please visit this page using a different browser.</p>
            </div>
        </div>

        <!-- error message -->
        <div id="error-display" class="row page panel panel-danger">
            <div class="col-sm-12">
                <div class="panel-heading">
                    <h3 class="panel-title" id="error-name"></h3>
                </div>
                <div class="panel-body">
                    <pre><code id="error-desc"></code></pre>
                </div>
            </div>
        </div>

        <!-- inbox display -->
        <div id="inbox" class="row page panel panel-default">
            <div class="col-sm-12">
                <div class="panel-heading">
                    <h1 class="panel-title">Inbox</h1>
                </div>
                <div id="inbox-status" class="panel-body">
                </div>
                <div class="list-group" id="message-list">
                </div>
            </div>
        </div>

        <!-- calendar display -->
        <div id="calendar" class="row page panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">Calendar</h1>
            </div>
            <div id="calendar-status" class="panel-body">
            </div>
            <div class="list-group" id="event-list">
            </div>
        </div>

        <!-- contacts display -->
        <div id="contacts" class="row page panel panel-default">
            <div class="col-sm-12">
                <div class="panel-heading">
                    <h1 class="panel-title">Contacts</h1>
                </div>
                <div id="contacts-status" class="panel-body">
                </div>
                <div class="list-group" id="contact-list">
                </div>
            </div>
        </div>

        <!-- token display -->
        <div id="token-display" class="row page panel panel-default">
            <div class="col-sm-12">
                <div class="panel-body">
                    <h4>Access Token:</h4>
                    <pre><code id="token"></code></pre>
                    <h4>Expires:</h4>
                    <p id="expires-display"></p>
                    <h4>ID Token:</h4>
                    <pre><code id="id-token"></code></pre>
                </div>
            </div>
        </div>
    </div>

    <script id="msg-list-template" type="text/x-handlebars-template">
        {{#each messages}}   
            <div class="list-group-item">
                <h3 id="msg-from" class="list-group-item-">{{this.from.emailAddress.name}}</h3>
                <h4 id="msg-subject" class="list-group-item-heading">{{this.subject}}</h4>
                <p id="msg-received" class="list-group-item-heading text-muted"><em>Received: {{formatDate this.receivedDateTime}}</em></p>
                <p id="msg-preview" class="list-group-item-text text-muted"><em>{{this.bodyPreview}}</em></p>
            </div>
        {{/each}}
 
    </script>

    <!-- Handlebars template for event list -->
    <script id="event-list-template" type="text/x-handlebars-template">
        {{#each events}} 
            <div class="row row-striped list-group-item">
                <div class="col-2 text-right">
                    <h1 class="display-4"><span class="badge badge-secondary">{{formatDate this.start.dateTime "dd"}}</span></h1>
                    <h2>{{formatDate this.start.dateTime "MMMM"}}</h2>
                </div>
                <div class="col-10">
                    <h3 id="event-subject" class="text-uppercase"><strong>{{this.subject}}</strong></h3>
                    <ul class="list-inline">
                        <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i>{{formatDate this.start.dateTime.day}}</li>
                        <li id="event-start" class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i>{{formatDate this.start.dateTime "hhmm tt"}} - {{formatDate this.end.dateTime "hhmm tt"}}</li>
                        <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i>{{this.location}}</li>
                    </ul>
                </div>
            </div>
        {{/each}}
 
    </script>

    <!-- Handlebars template for contact list -->
    <script id="contact-list-template" type="text/x-handlebars-template">
        {{#each contacts}}   
            <div class="list-group-item">
                <h4 id="contact-name" class="list-group-item-heading">{{this.givenName}} {{this.surname}}</h4>
                <p id="contact-email" class="list-group-item-heading">Email: {{this.emailAddresses.0.address}}</p>
            </div>
        {{/each}}
 
    </script>
    <script type="text/javascript">

        jQuery(document).ready(function () {
            //jQuery("#gallery").unitegallery();
           // jQuery("#event-list").unitegallery();

        });

	</script>

</asp:Content>
